# README #

Software for the Integrated Clinical Environment (ICE), a pure java IEEE 11073 SDC Family compatible API.

### What is this repository for? ###

* The master branch contains a stable version

### How do I get set up (Java)? ###

* **Download single all-in-one jar SoftICE.jar:** https://bitbucket.org/surgitaix/softice/downloads
* For a quick start, see the **snippets**: https://bitbucket.org/snippets/surgitaix/
* For more complex examples, see the sources: test/org/ornet/softice/test
* DemoProviderFactory.java contains a provider using metrics, alerts, contexts and streaming

### How to use SoftICE in .NET (C#, VB) ###
* **Download SoftICE.dll:** https://bitbucket.org/surgitaix/softice/downloads
* Get IKVM dependencies here: https://www.nuget.org/packages/IKVM/
* Add references to SoftICE.dll and IKVM DLLs in your project