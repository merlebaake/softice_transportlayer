package org.ornet.softice.test;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.ssl.SSLContexts;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.ornet.softice.SoftICE;
import org.ornet.softice.provider.SDCProvider;
import org.ornet.softice.test.classes.DemoProviderFactory;

/**
 *
 * @author besting
 */
public class ExampleProvider {
    
    static SDCProvider provider = null;
    
    public ExampleProvider() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        SoftICE.getInstance().startup();
        SoftICE.getInstance().setSchemaValidationEnabled(true);
//        try {
//            SoftICE.getInstance().setClientSSLContext(SSLContexts.custom().loadTrustMaterial(new File("c:\\truststore.jks"), "5t6z7u8i".toCharArray()).build());
//            SoftICE.getInstance().setServerSSLContext(SSLContexts.custom().loadKeyMaterial(new File("c:\\keystore.jks"), "5t6z7u8i".toCharArray(), "1q2w3e4r".toCharArray()).build());            
//        } catch (Exception ex) {
//            Logger.getLogger(ExampleProvider.class.getName()).log(Level.SEVERE, null, ex);
//        }                       
                      
        provider = DemoProviderFactory.getDemoProvider("UDI-1234567890");
        provider.startup();      
        System.out.println("Provider running...");
        DemoProviderFactory.DemoStreamStateHandler dsh = (DemoProviderFactory.DemoStreamStateHandler) provider.getHandler("handle_stream");
        while(true) {
            System.out.println("Sending stream packet...");
            dsh.setValueInternal(new double [] {1, 2, 3, 4, 5});
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(SDCTestMDPWS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
    }
       
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {

    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testMDIB() {
        assertEquals(provider.isRunning(), true);
    }
    
}
