/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.ornet.cdm.AbstractAlertDescriptor;
import org.ornet.cdm.AbstractContextDescriptor;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.ActivateOperationDescriptor;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertSignalDescriptor;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.ComponentActivation;
import org.ornet.cdm.AbstractDescriptor;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.MdState;
import org.ornet.cdm.MetricCategory;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.NumericMetricDescriptor;
import org.ornet.cdm.AbstractOperationDescriptor;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.ScoDescriptor;
import org.ornet.cdm.SetAlertStateOperationDescriptor;
import org.ornet.cdm.SetContextStateOperationDescriptor;
import org.ornet.cdm.SetStringOperationDescriptor;
import org.ornet.cdm.SetValueOperationDescriptor;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.ActivateOperationState;
import org.ornet.cdm.OperatingMode;
import org.ornet.cdm.SetAlertStateOperationState;
import org.ornet.cdm.SetContextStateOperationState;
import org.ornet.cdm.SetStringOperationState;
import org.ornet.cdm.SetValueOperationState;
import org.ornet.cdm.StringMetricDescriptor;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.softice.provider.SDCEndpoint;
import org.yads.java.structures.MaxHashMap;

public class SDCToolbox {
        
    private static final Map<String, String> operationTargetCache = Collections.synchronizedMap(new MaxHashMap<>(64));
    private static final Map<String, String> operationHandleCache = Collections.synchronizedMap(new MaxHashMap<>(64));
    
    private static String getKey(SDCEndpoint ep, String value) {
        return ep.getEndpointReference() + ":" + value;
    }
    
    public static String getOperationTargetForOperationHandle(SDCEndpoint ep, String operationHandle) {
        String res;
        if ((res = operationTargetCache.get(getKey(ep, operationHandle))) != null)
            return res;
        MdDescription mdd = ep.getMDDescription();
        for (MdsDescriptor nextMDS : mdd.getMds()) {
            if (!(nextMDS instanceof MdsDescriptor))
                continue;
            MdsDescriptor hMDS = (MdsDescriptor)nextMDS;
            if (hMDS.getSco() == null)
                continue;
            for (AbstractOperationDescriptor op : hMDS.getSco().getOperation()) {
                if (op.getHandle().equals(operationHandle)) {
                    operationTargetCache.put(getKey(ep, operationHandle), op.getOperationTarget());
                    return op.getOperationTarget();
                }
            }
        }
        return null;
    }   
    
    public static String getFirstOperationHandleForOperationTarget(SDCEndpoint ep, String operationTarget) {
        String res;
        if ((res = operationHandleCache.get(getKey(ep, operationTarget))) != null)
            return res;
        MdDescription mdd = ep.getMDDescription();
        for (MdsDescriptor nextMDS : mdd.getMds()) {
            if (nextMDS.getSco() == null)
                continue;
            for (AbstractOperationDescriptor op : nextMDS.getSco().getOperation()) {
                if (op.getOperationTarget().equals(operationTarget)) {
                    operationHandleCache.put(getKey(ep, operationTarget), op.getHandle());
                    return op.getHandle();
                }
            }
        }
        return null;
    }    

    public static AbstractMetricDescriptor findMetricDescritor(SDCEndpoint ep, String handle) {
        MdDescription mdd = ep.getMDDescription();
        for (MdsDescriptor nextMDS : mdd.getMds()) {
            for (VmdDescriptor nextVmd : nextMDS.getVmd())
                for (ChannelDescriptor nextChn : nextVmd.getChannel())
                    for (AbstractMetricDescriptor nextMet : nextChn.getMetric())  
                        if (nextMet.getHandle().equals(handle))
                            return nextMet;
        }
        return null;
    }
    
    public static AbstractMetricDescriptor findReferencedMetricDescritor(SDCEndpoint ep, AbstractState s) {
        return findMetricDescritor(ep, s.getDescriptorHandle());   	
    }    
    
    public static AbstractMetricState findMetricState(SDCEndpoint ep, String handle) {
        return findState(ep, handle, AbstractMetricState.class);
    }
    
    public static AbstractContextState findContextState(SDCEndpoint ep, String handle) {
        return findState(ep, handle, AbstractContextState.class);
    }

    public static <T extends AbstractState> T findState(SDCEndpoint ep, String handle, Class<T> type) {
        List<String> handles = new ArrayList<>();
        handles.add(handle);
        MdState mdState = ep.getMDState(handles);
        List<AbstractState> stateList = mdState.getState();
        if (stateList.isEmpty())
            return null;
        return type.cast(stateList.get(0));
    }    
    
    public static boolean isMetricChangeAllowed(SDCEndpoint ep, AbstractMetricState state) {
        AbstractMetricDescriptor md = findMetricDescritor(ep, state.getDescriptorHandle());       
        if (md == null)
            return false;
        if (md.getMetricCategory() == MetricCategory.MSRMT)
            return false;
        ComponentActivation ca = state.getActivationState();
        if (ca != null)
            return ca.equals(ComponentActivation.ON);
        return true;
    }
    
    static abstract class AlertDescriptorGrabber<T extends AbstractAlertDescriptor>{
        abstract List<T> getAlertDescriptors(AlertSystemDescriptor as);
    }
    
    private static <T extends AbstractAlertDescriptor> Set<T> collectAllAlertDescriptors(SDCEndpoint ep, AlertDescriptorGrabber<T> adg) {
        Set<T> descriptors = new HashSet<>();
        MdDescription mdd = ep.getMDDescription();
        for (MdsDescriptor mds : mdd.getMds()) {
            if (mds.getAlertSystem() != null)
                descriptors.addAll(adg.getAlertDescriptors(mds.getAlertSystem()));
            for (VmdDescriptor vmd : mds.getVmd()) {
                if (vmd.getAlertSystem() != null)
                    descriptors.addAll(adg.getAlertDescriptors(vmd.getAlertSystem()));                
            }                
        }
        return descriptors;
    }
    
    public static Set<AlertConditionDescriptor> collectAllAlertConditionDescriptors(SDCEndpoint ep) {
        return collectAllAlertDescriptors(ep, new AlertDescriptorGrabber<AlertConditionDescriptor>() {
            @Override
            List<AlertConditionDescriptor> getAlertDescriptors(AlertSystemDescriptor as) {
                return as.getAlertCondition();
            }
        });
    }
    
    public static Set<AlertSignalDescriptor> collectAllAlertSignalDescriptors(SDCEndpoint ep) {
        return collectAllAlertDescriptors(ep, new AlertDescriptorGrabber<AlertSignalDescriptor>() {
            @Override
            List<AlertSignalDescriptor> getAlertDescriptors(AlertSystemDescriptor as) {
                return as.getAlertSignal();
            }
        });
    }    
    
    public static void createOperationDescriptor(List<AbstractOperationState> operationStates, AbstractDescriptor descr, MdsDescriptor mds) {
        ScoDescriptor sco = mds.getSco();
        if (sco == null) {
            sco = new ScoDescriptor();
            sco.setHandle(mds.getHandle() + "_sco");
            mds.setSco(sco);
        }
        List<AbstractOperationDescriptor> opDescr = sco.getOperation();
        boolean opDescrFound = false;
        for (AbstractOperationDescriptor nextopDescr : opDescr) {
            if (nextopDescr.getOperationTarget().equals(descr.getHandle()))
                opDescrFound = true;
        }
        if (!opDescrFound) {
            SDCToolbox.createOperationDescriptor(descr, opDescr, operationStates);
        }
    }

    private static void createOperationDescriptor(AbstractDescriptor descr, List<AbstractOperationDescriptor> opDescr, List<AbstractOperationState> opStates) {
        AbstractOperationDescriptor aod = null;
        AbstractOperationState aos = null;
        if (descr instanceof NumericMetricDescriptor) {
            aod = new SetValueOperationDescriptor();
            aos = new SetValueOperationState();
        }
        else if (descr instanceof StringMetricDescriptor) {
            aod = new SetStringOperationDescriptor();
            aos = new SetStringOperationState();
        }
        else if (descr instanceof AbstractContextDescriptor) {
            aod = new SetContextStateOperationDescriptor();
            aos = new SetContextStateOperationState();
        }
        else if (descr instanceof AbstractAlertDescriptor) {
            aod = new SetAlertStateOperationDescriptor();
            aos = new SetAlertStateOperationState();
        }
        else if (descr instanceof ActivateOperationDescriptor) {
            opDescr.add((ActivateOperationDescriptor)descr);
            aos = new ActivateOperationState();
        }
        if (aos == null) {
            throw new RuntimeException("Operation state could not be created for descriptor handle = " + descr.getHandle());
        }
        aos.setDescriptorHandle(descr.getHandle() + "_sco");
        aos.setOperatingMode(OperatingMode.EN);
        opStates.add(aos);
        if (descr instanceof ActivateOperationDescriptor) {
            // Desciptor already defined and added by application
            return;
        }
        if (aod == null) {
            throw new RuntimeException("Operation descriptor could not be created for descriptor handle = " + descr.getHandle());
        }        
        aod.setHandle(descr.getHandle() + "_sco");
        aod.setOperationTarget(descr.getHandle());
        opDescr.add(aod);      
    }    

}
