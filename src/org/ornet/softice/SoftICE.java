/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ssl.SSLContext;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import org.apache.http.ssl.SSLContextBuilder;
import org.ornet.mdpws.MDPWSStreamingManager;
import org.ornet.softice.validation.CustomValidationHandler;
import org.ornet.softice.validation.LogValidationHandler;
import org.xml.sax.SAXException;
import org.yads.java.YADSFramework;
import org.yads.java.io.fs.FileResource;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.util.Log;

public class SoftICE {

    private static SoftICE instance;
    private static JAXBUtil jaxbUtil;
    private final AtomicInteger portStart = new AtomicInteger(20000);
    private static final AtomicBoolean schemaValidation = new AtomicBoolean(true);
    private String bindInterface;
    private static String lastSchemaValidationLog = new String();
    private SSLContext clientSSLContext;
    private SSLContext serverSSLContext;
   
    private static CustomValidationHandler customValidationHandler;
    
    private SoftICE() {
    	customValidationHandler = new CustomValidationHandler();
    }
    public static SoftICE getInstance() {
        if (instance == null) {
            Log.info("SoftICE 2.4.0b D10/2017/10/05 IEEE-11073 SDC Family Stack (C) SurgiTAIX AG");
            instance = new SoftICE();
            System.setProperty("com.sun.xml.internal.bind.v2.runtime.JAXBContextImpl.fastBoot", "true");
            Log.info("Initializing class loader context...");
            try {
                jaxbUtil = JAXBUtil.getInstance();
                jaxbUtil.setSkipParameterValueParsing(true);
                jaxbUtil.setStaticContext(JAXBContext.newInstance("org.ornet.cdm"));
                if (schemaValidation.get()) {
                    enableSchemaValidation();
                }
                else {
                    disableSchemaValidation();
                }
            } catch (JAXBException ex) {
                Logger.getLogger(SoftICE.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
            MDPWSStreamingManager.getInstance().init();
        }
        return instance;
    }
    
    private static void enableSchemaValidation() {
        try {
            Schema schema = getSchema();
            jaxbUtil.setValidationEventHandler(customValidationHandler, schema);
            // To enable logging, add an instance of LogValidationHandler
            customValidationHandler.addHandler(new LogValidationHandler()); 
            final String svl = "Schema validation is enabled.";
            if (!svl.equals(lastSchemaValidationLog))
                Log.info(lastSchemaValidationLog = svl);
        } catch (Exception ex) {
            Logger.getLogger(SoftICE.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
	public static Schema getSchema() throws SAXException {
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);            
		List<Source> sources = new ArrayList<>();
		sources.add(new StreamSource(FileResource.class.getResourceAsStream(SDCService.EXTENSION_POINTXSD)));
		sources.add(new StreamSource(FileResource.class.getResourceAsStream(SDCService.BICEPS__PARTICIPANT_MODELXSD)));
		sources.add(new StreamSource(FileResource.class.getResourceAsStream(SDCService.BICEPS__MESSAGE_MODELXSD)));
        sources.add(new StreamSource(FileResource.class.getResourceAsStream(SDCService.MDPWS_XSD)));
		Schema schema = sf.newSchema(sources.toArray(new Source[0]));
		return schema;
	}    
    
    private static void disableSchemaValidation() {
        final String svl = "Schema validation is disabled.";
        if (!svl.equals(lastSchemaValidationLog))
            Log.info(lastSchemaValidationLog = svl);
        jaxbUtil.setValidationEventHandler(null, null);
    }
    
    public void setSchemaValidationEnabled(boolean validate) {
        schemaValidation.set(validate);
        if (jaxbUtil != null) {
            if (validate) {
                enableSchemaValidation();
            }
            else {
                disableSchemaValidation();
            }
        }
    }

    public boolean isSchemaValidationEnabled() {
        return schemaValidation.get();
    }
    
    public int extractNextPort() {
        return portStart.incrementAndGet();
    }

    public void setPortStart(int portStart) {
        this.portStart.set(portStart);
    }   
    
    public void startup() {
        startup(Log.DEBUG_LEVEL_INFO, false);
    }

    public void setBindInterface(String bindInterface) {
        this.bindInterface = bindInterface;
    }

    public String getBindInterface() {
        return bindInterface;
    }
    
    public void startup(int logLevel, boolean logStackTrace) {
        Log.setLogLevel(logLevel);
        Log.setLogStackTrace(logStackTrace);
        Log.info("SoftICE startup...");
        YADSFramework.start(null);
    }
    
    public void shutdown() {
        Log.info("SoftICE shutdown...");
        final MDPWSStreamingManager sm = MDPWSStreamingManager.getInstance();
        sm.removeAllStreamListeners();
        sm.removeAllStreamSenders();
        YADSFramework.stop();
    }
    
    /**
     * Get an SSL context that trusts all certificates.
     * 
     * @return the trust-all context
     */    
    public SSLContext getTrustAllSSLContext() {
        try {
            return new SSLContextBuilder()
                    .loadTrustMaterial(null, (x509CertChain, authType) -> true)
                    .build();
        } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException ex) {
            Logger.getLogger(SoftICE.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Create default SSL context that trust all for client side.
     */    
    public void configureAllClientsTrustAll() {
        setClientSSLContext(getTrustAllSSLContext());
        setServerSSLContext(getTrustAllSSLContext());
    }

    /**
     * Get the SSL context used for client connections.
     * 
     * @return the providerClientSSLContext
     */
    public SSLContext getClientSSLContext() {
        return clientSSLContext;
    }

    /**
     * Set the SSL context used for client connections.
     * 
     * @param clientSSLContext the clientSSLContext to set
     */
    public void setClientSSLContext(SSLContext clientSSLContext) {
        this.clientSSLContext = clientSSLContext;
    }

    /**
     * Get the SSL context used for HTTP servers.
     * 
     * @return the serverSSLContext
     */
    public SSLContext getServerSSLContext() {
        return serverSSLContext;
    }

    /**
     * Set the SSL context used for server on provider side.
     * 
     * @param serverSSLContext the serverSSLContext to set
     */
    public void setServerSSLContext(SSLContext serverSSLContext) {
        this.serverSSLContext = serverSSLContext;
    }
    
}
