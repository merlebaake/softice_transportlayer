/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.math.BigInteger;
import java.util.List;
import org.ornet.cdm.GetContextStates;
import org.ornet.cdm.GetContextStatesResponse;
import org.ornet.cdm.GetMdDescriptionResponse;
import org.ornet.cdm.GetMdibResponse;
import org.ornet.cdm.GetMdState;
import org.ornet.cdm.GetMdStateResponse;
import org.ornet.cdm.Mdib;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.service.Operation;
import org.yads.java.service.parameter.ParameterValue;

public class ProviderGetOperationFactory {
    
    static InvokeDelegate createGetMDIBOperation(final SDCProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetMdibResponse response = new GetMdibResponse();
            response.setSequenceId("0");
            final Mdib mdib = provider.getMDIB();
            response.setMdib(mdib);
            response.setSequenceId(mdib.getSequenceId());
            response.setMdibVersion(mdib.getMdibVersion());
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }

    static InvokeDelegate createGetMDDescriptionOperation(final SDCProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetMdDescriptionResponse response = new GetMdDescriptionResponse();
            response.setSequenceId("0");
            response.setMdibVersion(BigInteger.valueOf(provider.getMdibVersion()));
            response.setMdDescription(provider.getMDDescription());
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }

    static InvokeDelegate createGetMDStateOperation(final SDCProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetMdState request = JAXBUtil.getInstance().createInputParameterValue(arguments, GetMdState.class);
            List<String> handles = request.getHandleRef();
            GetMdStateResponse response = new GetMdStateResponse();
            response.setSequenceId("0");
            response.setMdibVersion(BigInteger.valueOf(provider.getMdibVersion()));
            response.setMdState(handles == null || handles.isEmpty()? provider.getMDState() : provider.getMDState(handles));
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }
    
    static InvokeDelegate createGetContextsOperation(final SDCProvider provider) {
        return (Operation operation, ParameterValue arguments, CredentialInfo credentialInfo) -> {
            GetContextStates request = JAXBUtil.getInstance().createInputParameterValue(arguments, GetContextStates.class);
            List<String> handles = request.getHandleRef();
            GetContextStatesResponse response = new GetContextStatesResponse();
            response.setSequenceId("0");
            response.setMdibVersion(BigInteger.valueOf(provider.getMdibVersion()));
            response.getContextState().addAll(provider.getContextStates(handles));
            ParameterValue pv = JAXBUtil.getInstance().createOutputParameterValue(response);
            return pv;
        };
    }       
    
}
