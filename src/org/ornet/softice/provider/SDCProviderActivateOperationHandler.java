/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.ornet.cdm.InvocationState;
import org.ornet.cdm.Mdib;

public abstract class SDCProviderActivateOperationHandler  extends SDCProviderHandler {
    
    public SDCProviderActivateOperationHandler(String descriptorHandle) {
        super(descriptorHandle);
    }

    public InvocationState onActivateRequest(Mdib mdib, OperationInvocationContext oic) {
        return InvocationState.FAIL;
    }    
    
}
