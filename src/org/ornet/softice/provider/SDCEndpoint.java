/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import java.util.List;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.MdState;

/**
 *
 * @author besting
 */
public interface SDCEndpoint {

    /**
     * Get deep clone of MD description.
     *
     * @return The description.
     */
    MdDescription getMDDescription();

    /**
     * Create an MDIB container object. Modifications of this structure will not be reflected into the internal MDIB representation.
     *
     * @return The MDIB.
     */
    Mdib getMDIB();

    /**
     * Get deep clone of all MD states.
     *
     * @return The states.
     */
    MdState getMDState();

    /**
     * Get deep clone of MD states.
     *
     * @param handles List of handles to match.
     * @return The states.
     */
    MdState getMDState(List<String> handles);
    
    /**
     * Get ID of endpoint
     *
     * @return the ID.
     */
    String getEndpointReference();
    
}
