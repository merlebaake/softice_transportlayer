/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.ornet.cdm.AbstractState;
import org.ornet.cdm.InvocationState;

public class SDCFluentStateChangeRequestContext {
    
    private final AbstractState state;
    private final OperationInvocationContext oic;
    private final SDCFluentAutoSettableProviderHandler handler;

    public SDCFluentStateChangeRequestContext(AbstractState state, OperationInvocationContext oic, SDCFluentAutoSettableProviderHandler handler) {
        this.state = state;
        this.oic = oic;
        this.handler = handler;
    }

    public AbstractState getState() {
        return state;
    }

    public OperationInvocationContext getOperationInvocationContext() {
        return oic;
    }

    public void endNotify(InvocationState is) {
        handler.endNotify(is);
    }
    
}
