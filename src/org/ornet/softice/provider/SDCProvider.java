/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.softice.provider;

import org.ornet.softice.SDCToolbox;
import com.rits.cloning.Cloner;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertDescriptor;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextDescriptor;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.AbstractMetricState;
import org.ornet.cdm.AbstractOperationState;
import org.ornet.cdm.Activate;
import org.ornet.cdm.ActivateResponse;
import org.ornet.cdm.AlertConditionDescriptor;
import org.ornet.cdm.AlertConditionState;
import org.ornet.cdm.AbstractDescriptor;
import org.ornet.cdm.AbstractMetricDescriptor;
import org.ornet.cdm.GetMdibResponse;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.MdState;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.cdm.SetAlertState;
import org.ornet.cdm.SetAlertStateResponse;
import org.ornet.cdm.SetContextState;
import org.ornet.cdm.SetContextStateResponse;
import org.ornet.cdm.SetString;
import org.ornet.cdm.SetStringResponse;
import org.ornet.cdm.SetValue;
import org.ornet.cdm.SetValueResponse;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.AlertSystemDescriptor;
import org.ornet.cdm.ChannelDescriptor;
import org.ornet.cdm.Mdib;
import org.ornet.cdm.SystemContextDescriptor;
import org.ornet.cdm.VmdDescriptor;
import org.ornet.mdpws.MDPWSStreamingManager;
import org.ornet.softice.SDCDevice;
import org.ornet.softice.SoftICE;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.schema.JAXBUtil;
import org.yads.java.service.AppSequenceManager;
import org.yads.java.service.EventDelegate;
import org.yads.java.service.InvokeDelegate;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.URI;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.util.Log;

public final class SDCProvider implements SDCEndpoint {

    private final SDCDevice dev = new SDCDevice();
    private final AtomicLong mdibVersion = new AtomicLong();
    private static final ExecutorService executor = Executors.newCachedThreadPool();
    
    private MdDescription mddescription = new MdDescription();
    
    private final List<AbstractState> mdibStates = Collections.synchronizedList(new ArrayList<>());
    private final List<AbstractOperationState> operationStates = Collections.synchronizedList(new ArrayList<>());
    
    private final Map<String, SDCProviderHandler> providerHandlers = Collections.synchronizedMap(new HashMap<>());
    
    public SDCProvider() {
    }

    @Override
    @SuppressWarnings("FinalizeDeclaration")
    public void finalize() throws Throwable {
        try {
            if (isRunning()) {
                shutdown();
            }
        } finally {
            super.finalize();
        }
    }
       
    /**
     * Get deep clone of MD description.
     * 
     * @return The description.
     */
    @Override
    public MdDescription getMDDescription() {
        Cloner cloner = new Cloner();
        return cloner.deepClone(mddescription);
    }

    public long getMdibVersion() {
        return mdibVersion.get();
    }  
    
    public void setMDDescription(MdDescription mddescription){
    	this.mddescription = mddescription;
    }
    
    public void addMDS(MdsDescriptor mds) {
        boolean duplicate = false;
        for(MdsDescriptor descriptor : mddescription.getMds()) {
            if(descriptor.getHandle().equals(mds.getHandle())) {
                duplicate = true;
                break;
            }
        }
        if(!duplicate) {
            this.mddescription.getMds().add(mds);
            mdibVersion.incrementAndGet();
        }
    }
    
    public void removeMDS(String handle) {
        if(handle != null) {        	
        	int i = 0;
        	boolean match = false;
        	for (MdsDescriptor descriptor : mddescription.getMds()) {
        		if(descriptor.getHandle().equals(handle)) {
        			match = true; 
        			break;
        		}
        		i++;
        	}
        	// Remove
        	if(match) {
	        	mddescription.getMds().remove(i);        	
	        	mdibVersion.incrementAndGet();
        	}
        }
    }
    
    /**
     * Get deep clone of all MD states.
     * 
     * @return The states.
     */
    @Override
    public MdState getMDState() {
        MdState states = new MdState();
        Cloner cloner = new Cloner();
        synchronized(mdibStates) {
            states.getState().addAll(cloner.deepClone(mdibStates));
        }
        synchronized(operationStates) {
            states.getState().addAll(cloner.deepClone(operationStates));
        }
        return states;
    }
    
    public List<AbstractContextState> getContextStates() {
        return getContextStates(null);
    }
    
    public List<AbstractContextState> getContextStates(List<String> handles) {
        return getTypedStates(handles, AbstractContextState.class);
    } 
    
    public List<AbstractAlertState> getAlertStates() {
        return getTypedStates(null, AbstractAlertState.class);
    }     
    
    public List<AbstractAlertState> getAlertStates(List<String> handles) {
        return getTypedStates(handles, AbstractAlertState.class);
    } 

    private <T> List<T> getTypedStates(List<String> handles, Class<T> type) {
        MdState states = handles == null || handles.isEmpty()? getMDState() : getMDState(handles);
        List<T> targetStates = new LinkedList<>();
        Iterator<AbstractState> it = states.getState().iterator();
        while (it.hasNext()) {
            AbstractState next = it.next();
            if (type.isAssignableFrom(next.getClass())) {
                targetStates.add(type.cast(next));                
            }
        }
        return targetStates;
    }       
    
    /**
     * Get deep clone of MD states.
     * 
     * @param handles List of handles to match.
     * @return The states.
     */
    @Override
    public MdState getMDState(List<String> handles) {
        MdState states = new MdState();
        Set<String> handleSet = new HashSet<>(handles);
        synchronized(mdibStates) {
            addToStateContainer(handleSet, states, mdibStates.iterator());
        }   
        synchronized(operationStates) {
            addToStateContainer(handleSet, states, operationStates.iterator());
        }          
        return states;
    }

    private <T extends AbstractState> void addToStateContainer(Set<String> handleSet, MdState states, Iterator<T> it) {
        Cloner cloner = new Cloner();
        while (it.hasNext()) {
            T next = it.next();
            if (handleSet.contains(next.getDescriptorHandle())) {
                AbstractState cloned = cloner.deepClone(next);
                states.getState().add(cloned);
            }
        }
    }
    
    public void createSetOperationForDescriptor(AbstractDescriptor descr, MdsDescriptor mds) {
        SDCToolbox.createOperationDescriptor(operationStates, descr, mds);
    }
    
    /**
     * Create an MDIB container object. Modifications of this structure will not be reflected into the internal MDIB representation.
     * 
     * @return The MDIB.
     */
    @Override
    public final Mdib getMDIB() {
        Mdib mdib = new Mdib();
        mdib.setSequenceId("0");
        mdib.setMdibVersion(BigInteger.valueOf(getMdibVersion()));
        mdib.setMdState(getMDState());
        mdib.setMdDescription(getMDDescription());
        return mdib;
    }
   
    public void startup() {
        Log.info("Provider startup...");
        try {
            dev.init(createOperations(), createEventSources());
            synchronized(providerHandlers) {
                for (SDCProviderHandler next : providerHandlers.values()) {
                    if (next instanceof SDCProviderMDStateHandler) {
                        final AbstractState newState = setDefaultStateValues(((SDCProviderMDStateHandler)next).getInitalClonedState());
                        mdibStates.add(newState);
                        if (newState instanceof RealTimeSampleArrayMetricState) {
                            UnknownDataContainer streamMetadata = new UnknownDataContainer();
                            MDPWSStreamingManager.getInstance().addNewStreamSenderEndpoint(newState.getDescriptorHandle(), streamMetadata, this);
                            dev.getStreamService().addCustomMData(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID, streamMetadata);
                        }                    
                    }
                }
            }
            dev.start();
            // Check for valid provider MDIB
            if (SoftICE.getInstance().isSchemaValidationEnabled()) {
                GetMdibResponse response = new GetMdibResponse();
                response.setSequenceId("0");
                final Mdib mdib = getMDIB();
                mdib.setSequenceId("0");
                response.setMdib(mdib);
                response.setSequenceId(mdib.getSequenceId());
                JAXBUtil.getInstance().createOutputParameterValue(response);                            
            }
        } catch (Exception ex) {
            Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isRunning() {
        return dev.isRunning();
    }
    
    public void shutdown() {
        Log.info("Provider shutdown...");
        try {
            dev.stop();
            ProviderSetOperationFactory.shutdown();
            MDPWSStreamingManager.getInstance().removeStreamSenders(this);
        } catch (IOException ex) {
            Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private <T extends AbstractState> boolean updateInternalMatchingState(AbstractState newState, List<T> list) {
        AbstractState temp = null;
        synchronized(list) {
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
               AbstractState next = it.next();
               if (next.getDescriptorHandle().equals(newState.getDescriptorHandle())) {
                   temp = next;
                   break;
               }
            }
        }
        if (temp == null)
            return false;
        list.remove((T)temp);
        newState.setStateVersion(temp.getStateVersion().add(BigInteger.ONE));
        Cloner cloner = new Cloner();
        list.add((T)cloner.deepClone(setDefaultStateValues(newState)));
        mdibVersion.incrementAndGet();        
        return true;
    }    
    
    private boolean updateInternalMatchingState(AbstractState newState) {
        if (!updateInternalMatchingState(newState, mdibStates))
            return updateInternalMatchingState(newState, operationStates);
        return true;
    }
    
    public void updateState(AbstractState state) {
        if (!updateInternalMatchingState(state))
            throw new RuntimeException("No state to update with given descriptor handle (state handle): " + state.getDescriptorHandle());
        // update setable state cache
        ProviderSetOperationFactory.updateSetableStateCache(this, state);
        // Eventing
        if (state instanceof RealTimeSampleArrayMetricState) {
            MDPWSStreamingManager.getInstance().sendStreamPacket((RealTimeSampleArrayMetricState)state);
        }        
        else if (state instanceof AbstractMetricState) {
            ProviderEventSourceFactory.fireEpisodicMetricEventReport((AbstractMetricState) state, BigInteger.valueOf(mdibVersion.get()));
            evaluateAlertConditions(((AbstractMetricState) state));
        }
        else if (state instanceof AbstractContextState)
            ProviderEventSourceFactory.fireEpisodicContextChangedReport((AbstractContextState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractAlertState)
            ProviderEventSourceFactory.fireEpisodicAlertEventReport((AbstractAlertState) state, BigInteger.valueOf(mdibVersion.get()));
        else if (state instanceof AbstractOperationState)
            throw new RuntimeException("Eventing not yet supported for operation states, handle: " + state.getDescriptorHandle());
    }  
    
    public synchronized void notifyOperationInvoked(OperationInvocationContext oic, InvocationState is, String operationErrorMsg) {
        ProviderEventSourceFactory.fireOperationInvokedReport(this, oic, is, BigInteger.valueOf(mdibVersion.get()), operationErrorMsg);
    }
    
    public void setEndpointReference(String epr) {
        dev.setEndpointReference(new EndpointReference(new URI(epr)));
    }
    
    public void addHandler(SDCProviderHandler handler) {
        if (dev.isRunning()) 
            throw new RuntimeException("Provider is running!");
        handler.setProvider(this);
        providerHandlers.put(handler.getDescriptorHandle(), handler);
    }

    public void removeHandler(SDCProviderHandler handler) {
        if (dev.isRunning()) 
            throw new RuntimeException("Provider is running!");
        providerHandlers.remove(handler.getDescriptorHandle());
    }
    
    public SDCProviderHandler getHandler(String descriptorHandle) {
        return providerHandlers.get(descriptorHandle);
    }

    protected Map<String, SDCProviderHandler> getStateHandlers() {
        return providerHandlers;
    }
    
    /**
     * Create operations
     * 
     * @return Hashmap WSDL -> operation name -> invoke delegate
     */
    private HashMap<String, HashMap<String, InvokeDelegate>> createOperations() {
        HashMap<String, HashMap<String, InvokeDelegate>> map = new HashMap<>();
        // Get operations
        map.put(SDCDevice.GETSERVICEWSDL, new HashMap<>());
        map.get(SDCDevice.GETSERVICEWSDL).put("GetMdib", ProviderGetOperationFactory.createGetMDIBOperation(this));
        map.get(SDCDevice.GETSERVICEWSDL).put("GetMdDescription", ProviderGetOperationFactory.createGetMDDescriptionOperation(this));
        map.get(SDCDevice.GETSERVICEWSDL).put("GetMdState", ProviderGetOperationFactory.createGetMDStateOperation(this));
        // Set operations
        ProviderSetOperationFactory.init();
        map.put(SDCDevice.SETSERVICEWSDL, new HashMap<>());
        map.get(SDCDevice.SETSERVICEWSDL).put("SetValue", ProviderSetOperationFactory.createOSCPSetOperation(this, SetValue.class, SetValueResponse.class));
        map.get(SDCDevice.SETSERVICEWSDL).put("SetString", ProviderSetOperationFactory.createOSCPSetOperation(this, SetString.class, SetStringResponse.class));
        map.get(SDCDevice.SETSERVICEWSDL).put("Activate", ProviderSetOperationFactory.createOSCPSetOperation(this, Activate.class, ActivateResponse.class));
        map.get(SDCDevice.SETSERVICEWSDL).put("SetAlertState", ProviderSetOperationFactory.createOSCPSetOperation(this, SetAlertState.class, SetAlertStateResponse.class));
        // Context operations
        if (!map.containsKey(SDCDevice.CTXSERVICEWSDL))
            map.put(SDCDevice.CTXSERVICEWSDL, new HashMap<>());
        map.get(SDCDevice.CTXSERVICEWSDL).put("GetContextStates", ProviderGetOperationFactory.createGetContextsOperation(this));
        map.get(SDCDevice.CTXSERVICEWSDL).put("SetContextState", ProviderSetOperationFactory.createOSCPSetOperation(this, SetContextState.class, SetContextStateResponse.class));   
        return map;
    }
    
    /**
     * Create event sources
     * 
     * @return Hashmap WSDL -> event name -> event delegate
     */
    private HashMap<String, HashMap<String, EventDelegate>> createEventSources() {
        HashMap<String, HashMap<String, EventDelegate>> map = new HashMap<>();
        // Event operations
        map.put(SDCDevice.EVENTREPORTWSDL, new HashMap<>());
        map.get(SDCDevice.EVENTREPORTWSDL).put("EpisodicMetricReport", ProviderEventSourceFactory.createEpisodicMetricReportSource(this));
        map.get(SDCDevice.EVENTREPORTWSDL).put("OperationInvokedReport", ProviderEventSourceFactory.createOperationInvokedReportSource(this));
        map.get(SDCDevice.EVENTREPORTWSDL).put("EpisodicAlertReport", ProviderEventSourceFactory.createEpisodicAlertReportSource(this));
        if (!map.containsKey(SDCDevice.CTXSERVICEWSDL))
            map.put(SDCDevice.CTXSERVICEWSDL, new HashMap<>());
        map.get(SDCDevice.CTXSERVICEWSDL).put("EpisodicContextChangedReport", ProviderEventSourceFactory.createEpisodicContextReportSource(this));
        // MDPWS streaming
        map.put(SDCDevice.WAVEREPORTWSDL, new HashMap<>());
        map.get(SDCDevice.WAVEREPORTWSDL).put("WaveformStreamReport", ProviderEventSourceFactory.createStreamReportSource(this));
        return map;
    }   
    
   public String getManufacturer() {
        return dev.getManufacturer();
    }

    public void setManufacturer(String manufacturer) {
        this.dev.setManufacturer(manufacturer);
    }

    public String getModelName() {
        return dev.getModelName();
    }

    public void setModelName(String modelName) {
        this.dev.setModelName(modelName);
    }

    public String getFriendlyName() {
        return dev.getFriendlyName();
    }

    public void setFriendlyName(String friendlyName) {
        this.dev.setFriendlyName(friendlyName);
    }    

    private AbstractState setDefaultStateValues(AbstractState state) {
        if (state.getStateVersion() == null)
            state.setStateVersion(BigInteger.ZERO);
        if (state instanceof AbstractContextState) {
            AbstractContextState acs = (AbstractContextState)state;
            if (acs.getBindingMdibVersion() == null) {
                acs.setBindingMdibVersion(BigInteger.ZERO);
            }
        }
        return state;
    }
    
    public AppSequenceManager getAppSequencer() {
        return dev.getAppSequencer();
    }

    private void evaluateAlertConditions(AbstractMetricState state) {
        executor.execute(() -> {
            Set<AlertConditionDescriptor> acds = SDCToolbox.collectAllAlertConditionDescriptors(this);
            for (AlertConditionDescriptor next : acds) {
                if (next.getSource().contains(state.getDescriptorHandle())) {
                    SDCProviderHandler handler = getHandler(next.getHandle());
                    if (handler == null) {
                        Logger.getLogger(SDCProvider.class.getName()).log(Level.SEVERE, "Error in evaluating alert conditions. Handler missing for: {0}", state.getDescriptorHandle());
                        return;
                    }
                    if (handler instanceof SDCProviderAlertConditionStateHandler) {
                        List<AbstractAlertState> handlerStates = getAlertStates(new ArrayList<>(Arrays.asList(new String [] {next.getHandle()})));
                        if (handlerStates.size() > 0) {
                            ((SDCProviderAlertConditionStateHandler)handler).sourceHasChanged(state, (AlertConditionState)handlerStates.get(0));
                        }
                    }
                }
            }            
        });
    }

    @Override
    public String getEndpointReference() {
        return dev.getEndpointReference().getAddress().toString();
    }

    public void createFluentAutoSettableHandlers(Collection<AbstractState> states, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        // Metric states
        for (MdsDescriptor nextMDS : mddescription.getMds()) {
            for (VmdDescriptor nextVmd : nextMDS.getVmd())
                for (ChannelDescriptor nextChn : nextVmd.getChannel())
                    for (AbstractMetricDescriptor nextMet : nextChn.getMetric()) {
                        final AbstractState state = getStateByHandle(states, nextMet.getHandle());
                        if (state == null) {
                            Log.warn("State missing for metric handle: " + nextMet.getHandle() + ". Skipping state (inital value & SCO operation).");
                            continue;
                        }                      
                        if (SDCToolbox.isMetricChangeAllowed(this, (AbstractMetricState) state)) {
                            createSetOperationForDescriptor(nextMet, nextMDS);
                        }
                        addHandler(new SDCFluentAutoSettableProviderHandler(nextMet.getHandle(), state, deque));
                    } 
        }
        // Alert states
        for (MdsDescriptor mds : mddescription.getMds()) {
            handleAlertOperations(mds.getAlertSystem(), mds, states, deque);
            for (VmdDescriptor vmd : mds.getVmd()) {
                handleAlertOperations(vmd.getAlertSystem(), mds, states, deque);           
            }                
        }
        // Context states
        for (MdsDescriptor mds : mddescription.getMds()) {
            final SystemContextDescriptor systemContext = mds.getSystemContext();
            if (systemContext != null)
                handleContextOperations(systemContext.getLocationContext(), states, mds, deque);
        }
    }

    private AbstractState getStateByHandle(Collection<AbstractState> states, String handle) {
        Iterator<AbstractState> it = states.iterator();
        while (it.hasNext()) {
            final AbstractState next = it.next();
            if (next.getDescriptorHandle().equals(handle))
                return next;
        }
        return null;
    }

    private void handleAlertOperations(AlertSystemDescriptor alertSystem, MdsDescriptor mds, Collection<AbstractState> states, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        if (alertSystem == null)
            return;
        handleAlertOperationDescriptors(new ArrayList<>(alertSystem.getAlertCondition()), mds, states, deque);
        handleAlertOperationDescriptors(new ArrayList<>(alertSystem.getAlertSignal()), mds, states, deque);
    }

    private void handleAlertOperationDescriptors(List<AbstractAlertDescriptor> descriptors, MdsDescriptor mds, Collection<AbstractState> states, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        for (AbstractAlertDescriptor next : descriptors) {
            final AbstractState state = getStateByHandle(states, next.getHandle());
            if (state == null) {
                Log.warn("State missing for alert handle: " + next.getHandle() + ". Skipping SCO operation creation.");
                continue;
            }
            createSetOperationForDescriptor(next, mds);
            addHandler(new SDCFluentAutoSettableProviderHandler(next.getHandle(), state, deque));      
        }
    }

    private void handleContextOperations(AbstractContextDescriptor cd, Collection<AbstractState> states, MdsDescriptor mds, BlockingDeque<SDCFluentStateChangeRequestContext> deque) {
        final AbstractState state = getStateByHandle(states, cd.getHandle());
        if (state == null) {
            Log.warn("State missing for context handle: " + cd.getHandle() + ". Skipping SCO operation creation.");
            return;
        }
        createSetOperationForDescriptor(cd, mds);
        addHandler(new SDCFluentAutoSettableProviderHandler(cd.getHandle(), state, deque));           
    }

}
