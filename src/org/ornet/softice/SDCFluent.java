/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */

package org.ornet.softice;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.AbstractAlertState;
import org.ornet.cdm.AbstractContextState;
import org.ornet.cdm.InvocationState;
import org.ornet.cdm.MdDescription;
import org.ornet.cdm.MdState;
import org.ornet.cdm.NumericMetricState;
import org.ornet.cdm.NumericMetricValue;
import org.ornet.cdm.AbstractState;
import org.ornet.cdm.MdsDescriptor;
import org.ornet.cdm.StringMetricState;
import org.ornet.cdm.StringMetricValue;
import org.ornet.softice.consumer.FutureInvocationState;
import org.ornet.softice.consumer.SDCConsumer;
import org.ornet.softice.provider.SDCProvider;
import org.ornet.softice.provider.SDCFluentStateChangeRequestContext;
import org.yads.java.client.DefaultClient;
import org.yads.java.client.SearchManager;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.service.Device;
import org.yads.java.service.listener.DeviceListener;
import org.yads.java.service.reference.DeviceReference;
import org.yads.java.types.HelloData;
import org.yads.java.types.QName;
import org.yads.java.types.QNameSet;
import org.yads.java.types.SearchParameter;
import org.yads.java.util.Log;

public class SDCFluent {
    
    private static final ConcurrentMap<String, SDCConsumer> sdcConsumers = new ConcurrentHashMap<>();
    private static final ConcurrentMap<String, SDCProvider> sdcProviders = new ConcurrentHashMap<>();
    private static final ConcurrentMap<String, BlockingDeque<AbstractState>> events = new ConcurrentHashMap<>();
    private static ExecutorService exec = Executors.newCachedThreadPool();
    private static final int TIMEOUT_REMOVE = 60000;
    private static final int DEFAULT_PORT_START = 10000;
    private static final int DEFAULT_SCAN_INTERVAL = 20000;
    private static final Object discoverySynchronizer = new Object();

    private static boolean joined = false;
    private static Timer timer = new Timer();
    private static SoftICE instance;
    
    /**
     * Optionally configure fluent API (schema validation enabled).
     * 
     * @param portStart The port start
     * @param bindInterface The network IP (prefix)
     */
    public static void Configure(int portStart, String bindInterface) {
        Configure(portStart, bindInterface, true);
    }
    
    /**
     * Optionally configure fluent API.
     * 
     * @param portStart The port start
     * @param bindInterface The network IP (prefix)
     * @param schemaValidation Whether to use schema validation
     */
    public static void Configure(int portStart, String bindInterface, boolean schemaValidation) {
        if (instance == null)
            instance = SoftICE.getInstance();
        instance.setPortStart(portStart);
        instance.setBindInterface(bindInterface);
        instance.setSchemaValidationEnabled(schemaValidation);
    }
    
    /**
     * Join OSCP network. Optimizes faster discover when called early bevor using API.
     * 
     */
    public static void JoinSDCNetwork() {
        JoinSDCNetwork(true);
    }    
    
    /**
     * Join OSCP network. Optimizes faster discover when called early bevor using API.
     * 
     * @param enableSearch Whether to search for other devices
     */
    public static void JoinSDCNetwork(boolean enableSearch) {
        if (joined) {
            return;
        }
        if (instance == null) {
            instance = SoftICE.getInstance();        
            // Default config
            Configure(DEFAULT_PORT_START, "0.0.0.0");            
        }
        instance.startup();
        Log.info("OSCP Fluent API initializing...");
        if (enableSearch) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(getSearchTimerTask(), 0, DEFAULT_SCAN_INTERVAL);            
        }
        Log.info("Fluent API active.");
        joined = true;
    }

    private static TimerTask getSearchTimerTask() {
        return new TimerTask() {
            
            @Override
            public void run() {
                SearchParameter search = new SearchParameter();
                search.setDeviceTypes(new QNameSet(new QName("MedicalDevice", "http://p11073-10207/draft10/msg/2017/10/05")), DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
                SearchManager.searchDevice(search, getSearchCallback(), null);
            }

            private DefaultClient getSearchCallback() {
                DefaultClient client = new DefaultClient() {

                    @Override
                    public void helloReceived(HelloData helloData) {
                        final DeviceReference devRef = getDeviceReference(helloData);
                        if (!sdcConsumers.containsKey(devRef.getEndpointReference().getAddress().toString())) {
                            handleNewDeviceFound(devRef);                            
                        }
                    }
                    
                    @Override
                    public void deviceFound(DeviceReference devRef, SearchParameter search, String comManId) {
                        if (!sdcConsumers.containsKey(devRef.getEndpointReference().getAddress().toString())) {
                            devRef.addListener(new DeviceListener() {

                                @Override
                                public void deviceRunning(DeviceReference deviceRef) {
                                }

                                @Override
                                public void deviceCompletelyDiscovered(DeviceReference deviceRef) {
                                }

                                @Override
                                public void deviceBye(DeviceReference deviceRef) {
                                    synchronized (discoverySynchronizer) {
                                        final String epr = deviceRef.getEndpointReference().getAddress().toString();
                                        SDCConsumer consumer = sdcConsumers.get(epr);
                                        if (consumer != null) {
                                            consumer.close();
                                            sdcConsumers.remove(epr);                                                                                    
                                        }
                                    }
                                }

                                @Override
                                public void deviceChanged(DeviceReference deviceRef) {
                                }

                                @Override
                                public void deviceBuiltUp(DeviceReference deviceRef, Device device) {
                                }

                                @Override
                                public void deviceCommunicationErrorOrReset(DeviceReference deviceRef) {
                                    
                                }
                            });
                            handleNewDeviceFound(devRef);
                        }
                    }
                };   
                client.registerHelloListening();
                return client;
            }
        };
    }
    
    private static void handleNewDeviceFound(DeviceReference devRef) {
        synchronized (discoverySynchronizer) {
            final SDCConsumer sdcConsumer = new SDCConsumer(devRef);
            if (sdcConsumer.isConnected()) {
                final String epr = devRef.getEndpointReference().getAddress().toString();
                sdcConsumers.put(epr, sdcConsumer);
                BlockingDeque<AbstractState> eventDeque = events.get(epr);                                                    
                if (eventDeque != null) {
                    sdcConsumer.setEventDeque(eventDeque);
                } 
                sdcConsumer.setConnectionLostHandler((consumer) -> {
                    new Timer().schedule(new TimerTask() {

                        @Override
                        public void run() {
                            synchronized (discoverySynchronizer) {
                                if (!consumer.isConnected()) {
                                    consumer.close();
                                    sdcConsumers.remove(consumer.getEndpointReference());
                                }
                            }
                        }

                    }, TIMEOUT_REMOVE);
                });                                                                       
            }
        }
    }       
    
    /**
     * Leave OSCP network.
     * 
     */
    public static void LeaveSDCNetwork() {
        timer.cancel();
        sdcConsumers.values().stream().forEach((consumer) -> {
            consumer.close();
        });    
        sdcProviders.values().stream().forEach((provider) -> {
            provider.shutdown();
        });            
        sdcConsumers.clear();
        events.clear();
        exec.shutdownNow();
        exec = Executors.newCachedThreadPool();
        joined = false;
        if (instance == null)
            instance = SoftICE.getInstance(); 
        instance.shutdown();
    }
    
    /**
     * Get a snapshot list of currently available devices (endpoint references).
     * 
     * @return EPR list
     */
    public static Set<String> GetMembers() {
        return new HashSet<>(sdcConsumers.keySet());
    }
    
    /**
     * Create a new member (provider / device)
     * 
     * @param member EPR
     * @param mds MDS descriptor
     * @param states Initial states
     * @return Deque for state change request events
     */    
    public static BlockingDeque<SDCFluentStateChangeRequestContext> CreateLocalMember(String member, MdsDescriptor mds, Collection<AbstractState> states) {
        if (sdcProviders.containsKey(member))
            return null;
        SDCProvider p = new SDCProvider();
        p.setEndpointReference(member);
        p.addMDS(mds);
        BlockingDeque<SDCFluentStateChangeRequestContext> deque = new LinkedBlockingDeque<>();
        p.createFluentAutoSettableHandlers(states, deque);
        p.startup();
        sdcProviders.put(member, p);
        // Wait for consumer counterpart
        long timeout = 5000;
        while (!sdcConsumers.containsKey(member) && timeout > 0) {
            try {
                Thread.sleep(50);
                timeout-=50;
            } catch (InterruptedException ex) {
                Logger.getLogger(SDCFluent.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return deque;
    }
    
    /**
     * Delete a new member (provider / device)
     * 
     * @param member EPR
     */     
    public static void DeleteLocalMember(String member) {
        SDCProvider p = sdcProviders.get(member);
        if (p == null)
            return;
        p.shutdown();
        sdcProviders.remove(member);
    }
    
    /**
     * Set state value on local member (provider / device).
     * 
     * @param member The EPR
     * @param state The state to update
     */
    public static void UpdateLocalState(String member, AbstractState state) {
        SDCProvider p = sdcProviders.get(member);
        if (p == null)
            return;
        p.updateState(state);
    }
    
    /**
     * Get the description of a member.
     * 
     * @param member The member
     * @return The description or null
     */
    public static MdDescription GetDescription(String member) {
        if (!sdcConsumers.containsKey(member))
            return null;
        return sdcConsumers.get(member).getMDDescription();
    }

    /**
     * Get all states of a member.
     * 
     * @param member The member
     * @return The states or null
     */
    public static MdState GetStates(String member) {
        return GetStates(member, null);
    }

    /**
     * Get states of a member.
     * 
     * @param member The member
     * @param handles The list of handles (or null, for all)
     * @return The states or null
     */    
    public static MdState GetStates(String member, List<String> handles) {
        if (!sdcConsumers.containsKey(member))
            return null;
        return sdcConsumers.get(member).getMDState(handles);
    }
    
    private static <T extends AbstractState> T GetState(String member, String handle, Class<T> stateType) {
        if (!sdcConsumers.containsKey(member))
            return null;
        return sdcConsumers.get(member).requestState(handle, stateType);
    }    
    
    /**
     * Get a specific state.
     * 
     * @param member The member.
     * @param handle The state or descriptor handle.
     * @return The state (or null)
     */
    public static NumericMetricState GetNumericState(String member, String handle) {
        return GetState(member, handle, NumericMetricState.class);
    }
    
    /**
     * Get a specific state.
     * 
     * @param member The member.
     * @param handle The state or descriptor handle.
     * @return The state (or null)
     */
    public static StringMetricState GetStringState(String member, String handle) {
        return GetState(member, handle, StringMetricState.class);
    }    
    
    /**
     * Get a specific state.
     * 
     * @param member The member.
     * @param handle The state or descriptor handle.
     * @return The state (or null)
     */
    public static AbstractContextState GetContextState(String member, String handle) {
        return GetState(member, handle, AbstractContextState.class);
    }    
    
    /**
     * Get a specific state.
     * 
     * @param member The member.
     * @param handle The state or descriptor handle.
     * @return The state (or null)
     */
    public static AbstractAlertState GetAlertState(String member, String handle) {
        return GetState(member, handle, AbstractAlertState.class);
    }    
    
    /**
     * Get a specific numeric value.
     * 
     * @param member The member.
     * @param handle The state or descriptor handle.
     * @return The value (or Double.NaN)
     */
    public static double GetSimpleNumberValue(String member, String handle) {
        NumericMetricState state = GetState(member, handle, NumericMetricState.class);
        if (state == null)
            return Double.NaN;
        if (state.getMetricValue() == null)
            return Double.NaN;
        NumericMetricValue nv = state.getMetricValue();
        if (nv.getValue() == null)
            return Double.NaN;
        return nv.getValue().doubleValue();
    }        
    
    /**
     * Get a specific string value.
     * 
     * @param member The member.
     * @param handle The state or descriptor handle.
     * @return The string (or null)
     */
    public static String GetSimpleStringValue(String member, String handle) {
        StringMetricState state = GetState(member, handle, StringMetricState.class);
        if (state == null)
            return null;
        if (state.getMetricValue() == null)
            return null;
        return state.getMetricValue().getValue();
    }     
      
    /**
     * Set a state number value on a member.
     * 
     * @param member The member
     * @param descriptorHandle The descriptor handle
     * @param value The value
     * @param timeout The timeout
     * @return True, in case of success
     */    
    public static boolean SetSimpleNumberValue(String member, String descriptorHandle, double value, int timeout) {
        SDCConsumer consumer = sdcConsumers.get(member);
        if (consumer == null)
            return false;
        FutureInvocationState fis = new FutureInvocationState();
        NumericMetricValue nv = new NumericMetricValue();
        nv.setValue(BigDecimal.valueOf(value));
        consumer.commitValue(descriptorHandle, nv, fis);
        return fis.waitReceived(InvocationState.FIN, timeout);
    } 
    
    /**
     * Set a state string value on a member.
     * 
     * @param member The member
     * @param descriptorHandle The descriptor handle
     * @param value The value
     * @param timeout The timeout
     * @return True, in case of success
     */     
    public static boolean SetSimpleStringValue(String member, String descriptorHandle, String value, int timeout) {
        SDCConsumer consumer = sdcConsumers.get(member);
        if (consumer == null)
            return false;
        FutureInvocationState fis = new FutureInvocationState();
        StringMetricValue sv = new StringMetricValue();
        sv.setValue(value);
        consumer.commitString(descriptorHandle, sv, fis);
        return fis.waitReceived(InvocationState.FIN, timeout);
    }    
    
    /**
     * Set a state value on a member.
     * 
     * @param member The member
     * @param state The state
     * @param fis The future invocation state
     * @return InvocationState, the invocation state
     */      
    public static InvocationState SetState(String member, AbstractState state, FutureInvocationState fis) {
        SDCConsumer consumer = sdcConsumers.get(member);
        if (consumer == null)
            return InvocationState.FAIL;
        return consumer.commitState(state, fis);
    }
    
    /**
     * Enable catch-all events and alarms for a member. Can be called even if the device hasn't been found yet.
     * 
     * @param member The member
     * @return Blocking Deque containing current events and alarms.
     */
    public static BlockingDeque<AbstractState> EnableEventing(String member) {
        BlockingDeque<AbstractState> eventDeque = events.get(member); 
        if (eventDeque == null) {
            eventDeque = new LinkedBlockingDeque<>();
            events.put(member, eventDeque);
            SDCConsumer consumer = sdcConsumers.get(member);
            if (consumer != null && consumer.isConnected()) {
                consumer.setEventDeque(eventDeque);
            }
            return eventDeque;
        } else {
            return eventDeque;
        }    
    }  
    
    /**
     * Handle events (provider -> consumer>.
     * 
     * @param deque The deque
     * @param handler The handler
     */    
    public static void HandleAsyncEvents(BlockingDeque<AbstractState> deque, ISDCFluentAsyncEventHandler<AbstractState> handler) {
        handleAsync(deque, handler);
    }
    
    /**
     * Handle requests (consumer -> provider).
     * 
     * @param deque The deque
     * @param handler The handler
     */    
    public static void HandleAsyncRequests(BlockingDeque<SDCFluentStateChangeRequestContext> deque, ISDCFluentAsyncEventHandler<SDCFluentStateChangeRequestContext> handler) {
        handleAsync(deque, handler);
    }
    
    private static <T extends Object> void handleAsync(BlockingDeque<T> deque, ISDCFluentAsyncEventHandler<T> handler) {
        exec.execute(() -> {
            while(true) {
                while (deque.size() > 0 ) {
                    try {
                        T next = deque.take();
                        handler.handle(next);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(SDCFluent.class.getName()).log(Level.SEVERE, null, ex);
                        return;
                    }
                }                                
            }
        });
    }    

    /**
     * Get SDC consumer for a member
     * 
     * @param member The member
     * @return SDCConsumer The consumer
     */    
    public static SDCConsumer GetSDCConsumer(String member) {
        return sdcConsumers.get(member);
    }
    
    /**
     * Exchange XML raw message with remote MDPWS service operation
     * 
     * @param member The member
     * @param rawData The XML raw data (SOAP body)
     * @param serviceId The service ID (GetService, SetService, ContextService)
     * @param operationName The operation name (Activate, SetString, SetValue, SetAlertState, SetContextState, GetContextStates, GetMdib, GetMdState, GetMdDescription)
     * @return String The XML raw response
     */      
    public static String ExchangeRaw(String member, String rawData, String serviceId, String operationName) {
        SDCConsumer consumer = sdcConsumers.get(member);
        if (consumer == null)
            return null;
        return consumer.exchangeRaw(rawData, serviceId, operationName);
    }

}
