/**
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the Eclipse Pulic License version 2.0.
 * http://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.txt
 *
 */
/**
 * @author besting
 * @Copyright (C) SurgiTAIX AG
 */
package org.ornet.mdpws;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ornet.cdm.RealTimeSampleArrayMetricState;
import org.ornet.softice.SoftICE;
import org.ornet.softice.consumer.SDCConsumer;
import org.ornet.softice.provider.SDCProvider;
import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.connection.ip.IPNetworkDetection;
import org.yads.java.io.xml.ElementHandlerRegistry;
import org.yads.java.types.QName;
import org.yads.java.types.UnknownDataContainer;

public class MDPWSStreamingManager {
 
    protected static final String STREAMING_NAMESPACE = "http://standards.ieee.org/downloads/11073/11073-20702-201x/";
    protected static final String MESSAGE_MODEL_NS = "http://p11073-10207/draft10/msg/2017/10/05";
    protected static final String TARGET_NAMESPACE = MESSAGE_MODEL_NS + "/WaveformStreamService";
    protected static final String STREAM_TYPE = "http://docs.oasis-open.org/ws-dd/soapoverudp/1.1/os/wsdd-soapoverudp-1.1-spec-os.html";
    
    public static final String ACTION_URI = MESSAGE_MODEL_NS + "/WaveformStreamService/WaveformStream";
    protected static final String MCAST_IP = "239.239.239.235";
    protected static final String PROTOCOL_PREFIX = "soap.udp://";
    
    public static final QName STREAM_DESCRIPTIONS = new QName("StreamDescriptions", STREAMING_NAMESPACE);
    
    private static MDPWSStreamingManager instance;
    private boolean initialized;
    private final Set<StreamReceiver> receivers = Collections.newSetFromMap(new ConcurrentHashMap<StreamReceiver, Boolean>());
    private final Map<String, StreamSender> senders = new ConcurrentHashMap<>();
    private ExecutorService executor = Executors.newCachedThreadPool();
    
    public static MDPWSStreamingManager getInstance() {
        if (instance == null) {
            instance = new MDPWSStreamingManager();
        }
        return instance;
    }
    
    public void init() {
        if (!initialized) {
            initMDPWSStreamingMEX();
            initialized = true;            
        }
    }    
    
    public void addNewStreamSenderEndpoint(String handle, UnknownDataContainer udc, SDCProvider provider) {
        QName sd = new QName("StreamDescriptions", STREAMING_NAMESPACE);
        List<String> ues = udc.getUnknownElements(sd);
        if (ues == null) {
            ues = new ArrayList<>();
        }
        final int newPort = SoftICE.getInstance().extractNextPort();
        ues.add(PROTOCOL_PREFIX + MCAST_IP + ":" + newPort);
        udc.addUnknownElement(sd, ues);
        Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
        List<DatagramSocket> outSockets = new ArrayList<>();
        final String bi = SoftICE.getInstance().getBindInterface();
        while (adrIter.hasNext()) {
            try {
                IPAddress next = adrIter.next();
                if (bi == null || next.getAddressWithoutNicId().startsWith(bi) || bi.equals("0.0.0.0")) {                
                    DatagramSocket socket = new DatagramSocket(null);
                    socket.bind(new InetSocketAddress(next.getAddressWithoutNicId(), 0));
                    outSockets.add(socket);                    
                }
            } catch (SocketException ex) {
                Logger.getLogger(MDPWSStreamingManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            senders.put(handle, new StreamSender(outSockets, InetAddress.getByName(MCAST_IP), newPort, provider));
        } catch (UnknownHostException ex) {
            Logger.getLogger(MDPWSStreamingManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sendStreamPacket(RealTimeSampleArrayMetricState newState) {
        StreamSender sender = senders.get(newState.getDescriptorHandle());
        if (sender == null)
            throw new RuntimeException("Can't send out stream packet: sender for descriptor handle not found: " + newState.getDescriptorHandle());
        sender.sendAll(newState);
    }    

    private void initMDPWSStreamingMEX() {
        ElementHandlerRegistry.getRegistry().registerElementHandler(STREAM_DESCRIPTIONS, StreamMetadataSerializer.StreamMEXSerializer());
    }
    
    public void addStreamListeners(SDCConsumer callback, List<String> addresses) {
        for (String nextAdr : addresses) {
            try {
                String address = nextAdr.substring(PROTOCOL_PREFIX.length());
                final int sep = address.indexOf(":");
                String ipStr = address.substring(0, sep);
                String portStr = address.substring(sep + 1);
                InetAddress group = InetAddress.getByName(ipStr);
                Iterator<IPAddress> adrIter = IPNetworkDetection.getInstance().getIPv4Addresses(true);
                final String bi = SoftICE.getInstance().getBindInterface();
                while (adrIter.hasNext()) {
                    MulticastSocket socket = new MulticastSocket(Integer.parseInt(portStr));
                    IPAddress next = adrIter.next();
                    if (bi == null || next.getAddressWithoutNicId().startsWith(bi) || bi.equals("0.0.0.0")) {
                        socket.setInterface(InetAddress.getByName(next.getAddressWithoutNicId()));
                        socket.joinGroup(group);
                        executor.execute(new StreamReceiver(callback, socket));                        
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(MDPWSStreamingManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void removeStreamListeners(SDCConsumer callback) {
        Iterator<StreamReceiver> it = receivers.iterator();
        synchronized(receivers) {
            while (it.hasNext()) {
               StreamReceiver next = it.next();
                if (next.getConsumer() == callback) {
                    next.interrupt();
                    it.remove();
                }
            }
        }
    }
    
    public void removeAllStreamListeners() {
        executor.shutdownNow();
        executor = Executors.newCachedThreadPool();
    }
    
    public void removeStreamSenders(SDCProvider provider) {
        Iterator<Map.Entry<String, StreamSender>> it = senders.entrySet().iterator();
        synchronized(senders) {
            while (it.hasNext()) {
               Map.Entry<String, StreamSender> next = it.next();
                if (next.getValue().getProvider() == provider) {
                    next.getValue().close();
                    it.remove();
                }
            }            
        }
    }    

    public void removeAllStreamSenders() {
        senders.clear();
    }

}
