//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReport"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ReportPart" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReportPart"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="InvocationInfo" type="{http://p11073-10207/draft10/msg/2017/10/05}InvocationInfo"/&gt;
 *                   &lt;element name="InvocationSource" type="{http://p11073-10207/draft10/pm/2017/10/05}InstanceIdentifier"/&gt;
 *                 &lt;/sequence&gt;
 *                 &lt;attribute name="OperationHandleRef" use="required" type="{http://p11073-10207/draft10/pm/2017/10/05}HandleRef" /&gt;
 *                 &lt;attribute name="OperationTarget" type="{http://p11073-10207/draft10/pm/2017/10/05}HandleRef" /&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "reportPart"
})
@XmlRootElement(name = "OperationInvokedReport", namespace = "http://p11073-10207/draft10/msg/2017/10/05")
public class OperationInvokedReport
    extends AbstractReport
{

    @XmlElement(name = "ReportPart", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
    protected List<OperationInvokedReport.ReportPart> reportPart;

    /**
     * Gets the value of the reportPart property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reportPart property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReportPart().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OperationInvokedReport.ReportPart }
     * 
     * 
     */
    public List<OperationInvokedReport.ReportPart> getReportPart() {
        if (reportPart == null) {
            reportPart = new ArrayList<OperationInvokedReport.ReportPart>();
        }
        return this.reportPart;
    }


    /**
     * <p>Java-Klasse für anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://p11073-10207/draft10/msg/2017/10/05}AbstractReportPart"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="InvocationInfo" type="{http://p11073-10207/draft10/msg/2017/10/05}InvocationInfo"/&gt;
     *         &lt;element name="InvocationSource" type="{http://p11073-10207/draft10/pm/2017/10/05}InstanceIdentifier"/&gt;
     *       &lt;/sequence&gt;
     *       &lt;attribute name="OperationHandleRef" use="required" type="{http://p11073-10207/draft10/pm/2017/10/05}HandleRef" /&gt;
     *       &lt;attribute name="OperationTarget" type="{http://p11073-10207/draft10/pm/2017/10/05}HandleRef" /&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "invocationInfo",
        "invocationSource"
    })
    public static class ReportPart
        extends AbstractReportPart
    {

        @XmlElement(name = "InvocationInfo", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
        protected InvocationInfo invocationInfo;
        @XmlElement(name = "InvocationSource", namespace = "http://p11073-10207/draft10/msg/2017/10/05", required = true)
        protected InstanceIdentifier invocationSource;
        @XmlAttribute(name = "OperationHandleRef", required = true)
        protected String operationHandleRef;
        @XmlAttribute(name = "OperationTarget")
        protected String operationTarget;

        /**
         * Ruft den Wert der invocationInfo-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link InvocationInfo }
         *     
         */
        public InvocationInfo getInvocationInfo() {
            return invocationInfo;
        }

        /**
         * Legt den Wert der invocationInfo-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link InvocationInfo }
         *     
         */
        public void setInvocationInfo(InvocationInfo value) {
            this.invocationInfo = value;
        }

        /**
         * Ruft den Wert der invocationSource-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link InstanceIdentifier }
         *     
         */
        public InstanceIdentifier getInvocationSource() {
            return invocationSource;
        }

        /**
         * Legt den Wert der invocationSource-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link InstanceIdentifier }
         *     
         */
        public void setInvocationSource(InstanceIdentifier value) {
            this.invocationSource = value;
        }

        /**
         * Ruft den Wert der operationHandleRef-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperationHandleRef() {
            return operationHandleRef;
        }

        /**
         * Legt den Wert der operationHandleRef-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperationHandleRef(String value) {
            this.operationHandleRef = value;
        }

        /**
         * Ruft den Wert der operationTarget-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOperationTarget() {
            return operationTarget;
        }

        /**
         * Legt den Wert der operationTarget-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOperationTarget(String value) {
            this.operationTarget = value;
        }

    }

}
