//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A state of a limit ALERT CONDITION.
 * 
 * <p>Java-Klasse für LimitAlertConditionState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="LimitAlertConditionState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AlertConditionState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Limits" type="{http://p11073-10207/draft10/pm/2017/10/05}Range"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="MonitoredAlertLimits" use="required" type="{http://p11073-10207/draft10/pm/2017/10/05}AlertConditionMonitoredLimits" /&gt;
 *       &lt;attribute name="AutoLimitActivationState" type="{http://p11073-10207/draft10/pm/2017/10/05}AlertActivation" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LimitAlertConditionState", propOrder = {
    "limits"
})
public class LimitAlertConditionState
    extends AlertConditionState
{

    @XmlElement(name = "Limits", required = true)
    protected Range limits;
    @XmlAttribute(name = "MonitoredAlertLimits", required = true)
    protected AlertConditionMonitoredLimits monitoredAlertLimits;
    @XmlAttribute(name = "AutoLimitActivationState")
    protected AlertActivation autoLimitActivationState;

    /**
     * Ruft den Wert der limits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Range }
     *     
     */
    public Range getLimits() {
        return limits;
    }

    /**
     * Legt den Wert der limits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Range }
     *     
     */
    public void setLimits(Range value) {
        this.limits = value;
    }

    /**
     * Ruft den Wert der monitoredAlertLimits-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertConditionMonitoredLimits }
     *     
     */
    public AlertConditionMonitoredLimits getMonitoredAlertLimits() {
        return monitoredAlertLimits;
    }

    /**
     * Legt den Wert der monitoredAlertLimits-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertConditionMonitoredLimits }
     *     
     */
    public void setMonitoredAlertLimits(AlertConditionMonitoredLimits value) {
        this.monitoredAlertLimits = value;
    }

    /**
     * Ruft den Wert der autoLimitActivationState-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertActivation }
     *     
     */
    public AlertActivation getAutoLimitActivationState() {
        return autoLimitActivationState;
    }

    /**
     * Legt den Wert der autoLimitActivationState-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertActivation }
     *     
     */
    public void setAutoLimitActivationState(AlertActivation value) {
        this.autoLimitActivationState = value;
    }

}
