//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse für SafetyClassification.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="SafetyClassification"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="Inf"/&gt;
 *     &lt;enumeration value="MedA"/&gt;
 *     &lt;enumeration value="MedB"/&gt;
 *     &lt;enumeration value="MedC"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "SafetyClassification")
@XmlEnum
public enum SafetyClassification {


    /**
     * Inf = Informational. The descriptor and the related state information are intended to be used for information purposes only. They are not intended to be used in clinical functions.
     * 
     */
    @XmlEnumValue("Inf")
    INF("Inf"),

    /**
     * MedA = Medical Class A. The descriptor and related state information are intended to be used in clinical functions, specifically for general display in order to support patient and device monitoring. The displayed data is not intended to be used as sole source for diagnostic or therapeutic decisions. Deviations from this intended use are in the sole responsibility of the SERVICE CONSUMER.
     * 
     */
    @XmlEnumValue("MedA")
    MED_A("MedA"),

    /**
     * MedB = Medical Class B. The descriptor and related state information are intended to be used in clinical functions. The manufacturer has specified and considered a specific intended use for the data, which could result in non-serious injury. Deviations from this intended use are in the sole responsibility of the SERVICE CONSUMER.
     * 
     */
    @XmlEnumValue("MedB")
    MED_B("MedB"),

    /**
     * MedC = Medical Class C. The descriptor and related state information are intended to be used in clinical functions. The manufacturer has specified and considered a specific intended use for the data, which could result in serious injury. Deviations from this intended use are in the sole responsibility of the SERVICE CONSUMER.
     * 
     */
    @XmlEnumValue("MedC")
    MED_C("MedC");
    private final String value;

    SafetyClassification(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static SafetyClassification fromValue(String v) {
        for (SafetyClassification c: SafetyClassification.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
