//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * AlertSignalDescriptor represents an ALERT SIGNAL. An ALERT SIGNAL contains information about the way an ALERT CONDITION is communicated to a human. It is generated by an ALERT SYSTEM to indicate the presence or occurrence of an ALERT CONDITION.
 * 
 * Example: a signal could be a lamp (see pm:AlertSignalDescriptor/pm:Manifestation) on a remote POC MEDICAL DEVICE, such as the nurses handheld device (see pm:AlertSignalDescriptor/pm:SignalDelegationSupported), which starts flashing when the heart rate is exceeding 150bmp (see pm:AlertSignalDescriptor/pm:ConditionSignaled) for more than 2 seconds (see pm:AlertSignalDescriptor/pm:DefaultSignalGenerationDelay), and keeps flashing until the nurse confirms the alarm, even if the alarm condition is not present anymore (see pm:AlertSignalDescriptor/pm:Latching).
 * 
 * <p>Java-Klasse für AlertSignalDescriptor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AlertSignalDescriptor"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractAlertDescriptor"&gt;
 *       &lt;attribute name="ConditionSignaled" type="{http://p11073-10207/draft10/pm/2017/10/05}HandleRef" /&gt;
 *       &lt;attribute name="Manifestation" use="required" type="{http://p11073-10207/draft10/pm/2017/10/05}AlertSignalManifestation" /&gt;
 *       &lt;attribute name="Latching" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="DefaultSignalGenerationDelay" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="MinSignalGenerationDelay" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="MaxSignalGenerationDelay" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *       &lt;attribute name="SignalDelegationSupported" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AcknowledgementSupported" type="{http://www.w3.org/2001/XMLSchema}boolean" /&gt;
 *       &lt;attribute name="AcknowledgeTimeout" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AlertSignalDescriptor")
public class AlertSignalDescriptor
    extends AbstractAlertDescriptor
{

    @XmlAttribute(name = "ConditionSignaled")
    protected String conditionSignaled;
    @XmlAttribute(name = "Manifestation", required = true)
    protected AlertSignalManifestation manifestation;
    @XmlAttribute(name = "Latching", required = true)
    protected boolean latching;
    @XmlAttribute(name = "DefaultSignalGenerationDelay")
    protected Duration defaultSignalGenerationDelay;
    @XmlAttribute(name = "MinSignalGenerationDelay")
    protected Duration minSignalGenerationDelay;
    @XmlAttribute(name = "MaxSignalGenerationDelay")
    protected Duration maxSignalGenerationDelay;
    @XmlAttribute(name = "SignalDelegationSupported")
    protected Boolean signalDelegationSupported;
    @XmlAttribute(name = "AcknowledgementSupported")
    protected Boolean acknowledgementSupported;
    @XmlAttribute(name = "AcknowledgeTimeout")
    protected Duration acknowledgeTimeout;

    /**
     * Ruft den Wert der conditionSignaled-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConditionSignaled() {
        return conditionSignaled;
    }

    /**
     * Legt den Wert der conditionSignaled-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConditionSignaled(String value) {
        this.conditionSignaled = value;
    }

    /**
     * Ruft den Wert der manifestation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AlertSignalManifestation }
     *     
     */
    public AlertSignalManifestation getManifestation() {
        return manifestation;
    }

    /**
     * Legt den Wert der manifestation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AlertSignalManifestation }
     *     
     */
    public void setManifestation(AlertSignalManifestation value) {
        this.manifestation = value;
    }

    /**
     * Ruft den Wert der latching-Eigenschaft ab.
     * 
     */
    public boolean isLatching() {
        return latching;
    }

    /**
     * Legt den Wert der latching-Eigenschaft fest.
     * 
     */
    public void setLatching(boolean value) {
        this.latching = value;
    }

    /**
     * Ruft den Wert der defaultSignalGenerationDelay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getDefaultSignalGenerationDelay() {
        return defaultSignalGenerationDelay;
    }

    /**
     * Legt den Wert der defaultSignalGenerationDelay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setDefaultSignalGenerationDelay(Duration value) {
        this.defaultSignalGenerationDelay = value;
    }

    /**
     * Ruft den Wert der minSignalGenerationDelay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMinSignalGenerationDelay() {
        return minSignalGenerationDelay;
    }

    /**
     * Legt den Wert der minSignalGenerationDelay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMinSignalGenerationDelay(Duration value) {
        this.minSignalGenerationDelay = value;
    }

    /**
     * Ruft den Wert der maxSignalGenerationDelay-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getMaxSignalGenerationDelay() {
        return maxSignalGenerationDelay;
    }

    /**
     * Legt den Wert der maxSignalGenerationDelay-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setMaxSignalGenerationDelay(Duration value) {
        this.maxSignalGenerationDelay = value;
    }

    /**
     * Ruft den Wert der signalDelegationSupported-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSignalDelegationSupported() {
        return signalDelegationSupported;
    }

    /**
     * Legt den Wert der signalDelegationSupported-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSignalDelegationSupported(Boolean value) {
        this.signalDelegationSupported = value;
    }

    /**
     * Ruft den Wert der acknowledgementSupported-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAcknowledgementSupported() {
        return acknowledgementSupported;
    }

    /**
     * Legt den Wert der acknowledgementSupported-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAcknowledgementSupported(Boolean value) {
        this.acknowledgementSupported = value;
    }

    /**
     * Ruft den Wert der acknowledgeTimeout-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getAcknowledgeTimeout() {
        return acknowledgeTimeout;
    }

    /**
     * Legt den Wert der acknowledgeTimeout-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setAcknowledgeTimeout(Duration value) {
        this.acknowledgeTimeout = value;
    }

}
