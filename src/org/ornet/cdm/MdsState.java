//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Definition of the state of an pm:MdsDescriptor.
 * 
 * <p>Java-Klasse für MdsState complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="MdsState"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://p11073-10207/draft10/pm/2017/10/05}AbstractComplexDeviceComponentState"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="OperatingJurisdiction" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Lang" type="{http://www.w3.org/2001/XMLSchema}language" /&gt;
 *       &lt;attribute name="OperatingMode" type="{http://p11073-10207/draft10/pm/2017/10/05}MdsOperatingMode" /&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MdsState", propOrder = {
    "operatingJurisdiction"
})
public class MdsState
    extends AbstractComplexDeviceComponentState
{

    @XmlElement(name = "OperatingJurisdiction")
    protected Object operatingJurisdiction;
    @XmlAttribute(name = "Lang")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String lang;
    @XmlAttribute(name = "OperatingMode")
    protected MdsOperatingMode operatingMode;

    /**
     * Ruft den Wert der operatingJurisdiction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getOperatingJurisdiction() {
        return operatingJurisdiction;
    }

    /**
     * Legt den Wert der operatingJurisdiction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setOperatingJurisdiction(Object value) {
        this.operatingJurisdiction = value;
    }

    /**
     * Ruft den Wert der lang-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLang() {
        return lang;
    }

    /**
     * Legt den Wert der lang-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLang(String value) {
        this.lang = value;
    }

    /**
     * Ruft den Wert der operatingMode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link MdsOperatingMode }
     *     
     */
    public MdsOperatingMode getOperatingMode() {
        return operatingMode;
    }

    /**
     * Legt den Wert der operatingMode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link MdsOperatingMode }
     *     
     */
    public void setOperatingMode(MdsOperatingMode value) {
        this.operatingMode = value;
    }

}
