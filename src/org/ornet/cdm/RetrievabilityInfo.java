//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2017.10.20 um 12:27:03 PM CEST 
//


package org.ornet.cdm;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.Duration;


/**
 * Retrievability provides information on how it is possible to access a state.
 * 
 * <p>Java-Klasse für RetrievabilityInfo complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="RetrievabilityInfo"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;sequence&gt;
 *           &lt;element ref="{http://p11073-10207/draft10/ext/2017/10/05}Extension" minOccurs="0"/&gt;
 *         &lt;/sequence&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="Method" use="required" type="{http://p11073-10207/draft10/msg/2017/10/05}RetrievabilityMethod" /&gt;
 *       &lt;attribute name="UpdatePeriod" type="{http://www.w3.org/2001/XMLSchema}duration" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrievabilityInfo", namespace = "http://p11073-10207/draft10/msg/2017/10/05", propOrder = {
    "extension"
})
public class RetrievabilityInfo {

    @XmlElement(name = "Extension", namespace = "http://p11073-10207/draft10/ext/2017/10/05")
    protected ExtensionType extension;
    @XmlAttribute(name = "Method", required = true)
    protected RetrievabilityMethod method;
    @XmlAttribute(name = "UpdatePeriod")
    protected Duration updatePeriod;

    /**
     * Ruft den Wert der extension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtensionType }
     *     
     */
    public ExtensionType getExtension() {
        return extension;
    }

    /**
     * Legt den Wert der extension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtensionType }
     *     
     */
    public void setExtension(ExtensionType value) {
        this.extension = value;
    }

    /**
     * Ruft den Wert der method-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link RetrievabilityMethod }
     *     
     */
    public RetrievabilityMethod getMethod() {
        return method;
    }

    /**
     * Legt den Wert der method-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievabilityMethod }
     *     
     */
    public void setMethod(RetrievabilityMethod value) {
        this.method = value;
    }

    /**
     * Ruft den Wert der updatePeriod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getUpdatePeriod() {
        return updatePeriod;
    }

    /**
     * Legt den Wert der updatePeriod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setUpdatePeriod(Duration value) {
        this.updatePeriod = value;
    }

}
