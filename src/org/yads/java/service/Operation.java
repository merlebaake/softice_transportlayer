/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import org.yads.java.authorization.AuthorizationException;
import org.yads.java.communication.CommunicationException;
import org.yads.java.description.wsdl.WSDLOperation;
import org.yads.java.schema.ComplexType;
import org.yads.java.schema.Element;
import org.yads.java.schema.SimpleType;
import org.yads.java.security.CredentialInfo;
import org.yads.java.service.parameter.ParameterValue;
import org.yads.java.types.QName;

/**
 * An operation is an abstraction of executable code. Operations are (together
 * with {@link DefaultEventSource events}) the main parts of a service
 * implementation.
 * <p>
 * The actual business logic behind an operation is contained within the {@link #invoke(ParameterValue)} method. <code>Operation</code> subclasses are required to overwrite it providing the code to be executed when this operation is called.
 * </p>
 * <p>
 * Before adding an operation to a {@link DefaultService service}, the types of its {@link #getInput() input} and {@link #getOutput() output} parameters must be defined in terms of XML Schema constructs like {@link Element element}s, {@link SimpleType simple type}s and {@link ComplexType complex types}s. A simple operation with no input and a single string message as its only output parameter could look like:
 * 
 * <pre>
 * Operation myOperation = new Operation() {
 * 
 *     public ParameterValue invoke(ParameterValue params)
 *         throws InvocationException, TimeotException {
 *         // business logic goes here
 *         ...
 *     }
 * 
 * };
 * Element message = new Element(&quot;message&quot;,
 *     &quot;http://www.example.org/messageService&quot;, SchemaUtil.TYPE_STRING);
 * myOperation.setOutput(message);
 * </pre>
 * 
 * Additionally, if an operation's invocation can cause expected (checked) exceptional conditions (errors), they must be declared as {@link #addFault(Fault) faults}.
 * </p>
 * <strong>Note:</strong> According to <a href="http://www.w3.org/TR/wsdl">WSDL
 * 1.1 Specification</a>, an operation's {@link #getName() name} is not required
 * to be unique within the scope of its containing port type in order to support
 * overloading. However, when overloading operations, the combination of each
 * one's {@link #getName() name}, {@link #getInputName() input name} and {@link #getOutputName() output name} must be unique in order to avoid name
 * clashes. </p>
 */
public abstract class Operation extends OperationCommons {

	/**
	 * Creates a new operation instance with the given local <code>name</code> and <code>portType</code>.
	 * 
	 * @param name the name of the operation; see {@link OperationCommons here} for a short description of uniqueness requirements regarding
	 *            operation names
	 * @param portType the qualified port type of the operation
	 */
	public Operation(String name, QName portType) {
		super(name, portType);
	}

	/**
	 * Creates a new operation instance without specified name.
	 */
	public Operation() {
		super(null, null);
	}

	/**
	 * Creates a new operation instance with the given <code>name</code>.
	 * 
	 * @param name
	 */
	public Operation(String name) {
		super(name, null);
	}

	/**
	 * Creates a new operation instance with the given <code>name</code> and the
	 * name of the specified service. Namespace default is "http://www.yads.or"
	 * can be set once in the DefaultDevice (setDefaultNamespace). There is also
	 * the possibility to set the namespace for every operation.
	 * 
	 * @param name
	 */
	public Operation(String name, String serviceName) {
		super(name, new QName(serviceName, null));
	}

	protected Operation(WSDLOperation operation) {
		super(operation);
	}

	/**
	 * Returns the <code>transmission type</code> of this operation according to
	 * <a href="http://www.w3.org/TR/wsdl">WSDL 1.1 specification</a>. The value
	 * returned is one of {@link WSDLOperation#TYPE_ONE_WAY} or {@link WSDLOperation#TYPE_REQUEST_RESPONSE}.
	 * 
	 * @return type the transmission type of this operation
	 */
	@Override
	public final int getType() {
		if (type == WSDLOperation.TYPE_UNKNOWN) {
			/*
			 * this code handles only one-way and request-response operations,
			 * i.e. operations initiated from the client-side
			 */
			if (getOutput() == null && getFaultCount() == 0) {
				type = WSDLOperation.TYPE_ONE_WAY;
			} else {
				type = WSDLOperation.TYPE_REQUEST_RESPONSE;
			}
		}
		return type;
	}

	/**
	 * Returns <code>true</code> if the transmission type of this operation is {@link WSDLOperation#TYPE_ONE_WAY}. Returns <code>false</code> in any
	 * other case.
	 * 
	 * @return checks whether this is a {@link WSDLOperation#TYPE_ONE_WAY
	 *         one-way} operation
	 */
	public final boolean isOneWay() {
		return getType() == WSDLOperation.TYPE_ONE_WAY;
	}

	/**
	 * Returns <code>true</code> if the transmission type of this operation is {@link WSDLOperation#TYPE_REQUEST_RESPONSE}. Returns <code>false</code> in any other case.
	 * 
	 * @return checks whether this is a {@link WSDLOperation#TYPE_REQUEST_RESPONSE request-response} operation
	 */
	public final boolean isRequestResponse() {
		return getType() == WSDLOperation.TYPE_REQUEST_RESPONSE;
	}

	/**
	 * Method for invoke this Operation. Call with {@link ParameterValue} and {@link CredentialInfo}. If no security will be used, use <code>CredentialInfo.EMPTY_CREDENTIAL_INFO<code> instead of <code>null<code>.
	 * 
	 * @param parameterValue for invoke.
	 * @param credentialInfo if security is available.
	 * @return parameter value
	 * @throws InvocationException
	 * @throws CommunicationException
	 * @throws AuthorizationException
	 */
	public final ParameterValue invoke(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException, AuthorizationException {
		// if credentialInfo is null use EMPTY_CREDENTIAL_INFO
		if (credentialInfo == null) {
			credentialInfo = CredentialInfo.EMPTY_CREDENTIAL_INFO;
		}
		return invokeImpl(parameterValue, credentialInfo);
	}

	/**
	 * This method must be overwritten from User. This method will inherit the
	 * relevant code for invoking the operation.
	 * 
	 * @param parameterValue
	 * @param credentialInfo
	 * @return ParameterValue
	 * @throws InvocationException
	 * @throws CommunicationException
	 */
	protected abstract ParameterValue invokeImpl(ParameterValue parameterValue, CredentialInfo credentialInfo) throws InvocationException, CommunicationException;

}
