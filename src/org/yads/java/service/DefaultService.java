/*******************************************************************************
 * Copyright (c) 2016 Andreas Besting (info@besting-it.de)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/

package org.yads.java.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.yads.java.communication.CommunicationManager;
import org.yads.java.communication.CommunicationManagerRegistry;
import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.eventing.EventSource;
import org.yads.java.io.fs.FileResource;
import org.yads.java.types.URI;
import org.yads.java.util.Log;

public class DefaultService extends DefaultServiceBase {

	public DefaultService(String communicationManagerId) {
		super(communicationManagerId);
	}
	
	public Operation getOperation(String opName) {
		return getOperation(null, opName, null, null);
	}

    public EventSource getEventSource(String evName) {
        return getEventSource(null, evName, null, null);
    }
    
	public void registerFileResource(FileResource fileResource, String targetNamespace) throws Exception {
        Iterator<?> itBindings = hasCommunicationBindings() ? 
                getCommunicationBindings() : hasCommunicationAutoBindings() ? getCommunicationAutoBindings() : null;
        if (itBindings == null) {
            Log.error("No bindings found, can't register file resources!");
            return;
        }        
		String resourcesBasePath = "yads/resources/";
		ResourcePath rp = createResourcePath(targetNamespace, fileResource.getFileName());
        String resourcePath = rp.path;
		while (itBindings.hasNext()) {
			CommunicationBinding binding = (CommunicationBinding) itBindings.next();
			CommunicationManager manager = CommunicationManagerRegistry.getCommunicationManager(binding.getCommunicationManagerId());
			URI uri = manager.registerResource(fileResource, binding, resourcesBasePath + resourcePath);

			Set<URI> uris = resourceURIs.get(binding);
			if (uris == null) {
				uris = new HashSet<URI>();
				resourceURIs.put(binding, uris);
			}
			uris.add(uri);

			if (Log.isDebug()) {
				Log.debug("Service [ Resource = " + uri + " ]", Log.DEBUG_LAYER_APPLICATION);
			}
		}        
    }    
	
}
