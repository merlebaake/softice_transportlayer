/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import java.io.IOException;
import java.util.Collection;

import org.yads.java.YADSFramework;
import org.yads.java.communication.CommunicationException;
import org.yads.java.dispatch.EprInfoHandler;
import org.yads.java.dispatch.EprInfoHandler.EprInfoProvider;
import org.yads.java.eventing.ClientSubscriptionInternal;
import org.yads.java.eventing.EventSink;
import org.yads.java.eventing.EventingException;
import org.yads.java.security.CredentialInfo;
import java.util.Iterator;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.EprInfo;
import org.yads.java.types.EprInfoSet;
import org.yads.java.types.URI;
import org.yads.java.types.XAddressInfo;
import org.yads.java.util.Log;
import org.yads.java.util.TimedEntry;
import org.yads.java.util.WatchDog;

/**
 * Subscription class, manages a client-side subscription.
 */
public final class DefaultClientSubscription extends TimedEntry implements ClientSubscriptionInternal, EprInfoProvider {

	final EventSink			sink;

	final String			clientSubscriptionId;

	private long			timeoutTime;

	public EprInfoSet		subscriptionManagerEprInfos;

	private EprInfoHandler	subscriptionManagerEprInfoHandler;

	private EprInfoProvider	eprInfoProvider;

	private Service			service;

	private String			comManId;

	private CredentialInfo	credentialInfo;

	/**
	 * Constructor.
	 * 
	 * @param sink
	 * @param clientSubscriptionId
	 * @param serviceSubscriptionId
	 * @param duration
	 * @param servRef
	 */
	public DefaultClientSubscription(EventSink sink, String clientSubscriptionId, EndpointReference subscriptionManagerEpr, String comManId, long duration, Service service, CredentialInfo credentialInfo) {
		this.sink = sink;
		this.clientSubscriptionId = clientSubscriptionId;
		this.eprInfoProvider = null;

		if (subscriptionManagerEpr != null) {
			subscriptionManagerEprInfos = new EprInfoSet();
			EprInfo notifyTo = new EprInfo(subscriptionManagerEpr, comManId);
			subscriptionManagerEprInfos.add(notifyTo);

			subscriptionManagerEprInfoHandler = new EprInfoHandler(this);

		}
		this.comManId = comManId;
		if (duration != 0) {
			timeoutTime = System.currentTimeMillis() + duration;
			WatchDog.getInstance().register(this, duration);
		} else {
			timeoutTime = 0;
		}
		this.service = service;
		if (credentialInfo == null) {
			this.credentialInfo = CredentialInfo.EMPTY_CREDENTIAL_INFO;
		} else {
			this.credentialInfo = credentialInfo;
		}

		YADSFramework.addClientSubscription(this);
	}

	public DefaultClientSubscription(EventSink sink, String clientSubscriptionId, EprInfoProvider eprInfoProvider, Service service, CredentialInfo credentialInfo) {
		this.sink = sink;
		this.clientSubscriptionId = clientSubscriptionId;
		this.eprInfoProvider = eprInfoProvider;
		this.service = service;
		if (credentialInfo == null) {
			this.credentialInfo = CredentialInfo.EMPTY_CREDENTIAL_INFO;
		} else {
			this.credentialInfo = credentialInfo;
		}
	}

	@Override
	public void register(long duration, EndpointReference subscriptionManagerEpr, String comMgr) {
		this.comManId = comMgr;

		subscriptionManagerEprInfos = new EprInfoSet();
		EprInfo notifyTo = new EprInfo(subscriptionManagerEpr, comManId);
		subscriptionManagerEprInfos.add(notifyTo);
		subscriptionManagerEprInfoHandler = new EprInfoHandler(this);

		if (duration != 0) {
			timeoutTime = System.currentTimeMillis() + duration;
			WatchDog.getInstance().register(this, duration);
		} else {
			timeoutTime = 0;
		}
		YADSFramework.addClientSubscription(this);
	}

	@Override
	public Collection getOutgoingDiscoveryInfos() {
		return eprInfoProvider.getOutgoingDiscoveryInfos();
	}

	@Override
	public Iterator getEprInfos() {
		return subscriptionManagerEprInfos.iterator();
	}

	@Override
	public String getDebugString() {
		return "Client Subscription " + clientSubscriptionId;
	}

	@Override
	public String getClientSubscriptionId() {
		return clientSubscriptionId;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#getTimeoutTime()
	 */
	@Override
	public synchronized long getTimeoutTime() {
		return timeoutTime;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.ClientSubscription#getSubscriptionManagerXAddressInfo
	 * ()
	 */
	@Override
	public EprInfo getSubscriptionManagerAddressInfo() {
		try {
			return subscriptionManagerEprInfoHandler.getPreferredXAddressInfo();
		} catch (CommunicationException e) {
			if (Log.isError()) {
				Log.printStackTrace(e);
			}
			return null;
		}
	}

	@Override
	public XAddressInfo getNextXAddressInfoAfterFailureForSubscriptionManager(URI transportAddress, int syncHostedBlockVersion) throws CommunicationException {
		return subscriptionManagerEprInfoHandler.getNextXAddressInfoAfterFailure(transportAddress, syncHostedBlockVersion);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#getServiceSubscriptionId()
	 */
	@Override
	public String getServiceSubscriptionId() {
		if (subscriptionManagerEprInfos != null && subscriptionManagerEprInfos.size() > 0) {
			return ((EprInfo) subscriptionManagerEprInfos.iterator().next()).getEndpointReference().getReferenceParameters().getWseIdentifier();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.ClientSubscription#getCommunicationManagerId()
	 */
	@Override
	public String getCommunicationManagerId() {
		return comManId;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#getEventSink()
	 */
	@Override
	public EventSink getEventSink() {
		return sink;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#getServiceReference()
	 */
	@Override
	public Service getService() {
		return service;
	}

	// ----------------------- SUBCRIPTION HANDLING -----------------

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#renew(long)
	 */
	@Override
	public long renew(long duration) throws EventingException, IOException, CommunicationException {
		return service.renew(this, duration, credentialInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#unsubscribe()
	 */
	@Override
	public void unsubscribe() throws EventingException, IOException, CommunicationException {
		service.unsubscribe(this, credentialInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscription#getStatus()
	 */
	@Override
	public long getStatus() throws EventingException, IOException, CommunicationException {
		long duration = service.getStatus(this, credentialInfo);
		updateTimeoutTime(duration);
		return duration;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.eventing.ClientSubscriptionInternal#renewInternal(long)
	 */
	@Override
	public void renewInternal(long newDuration) {
		if (newDuration != 0) {
			WatchDog.getInstance().update(this, newDuration);
		} else {
			WatchDog.getInstance().unregister(this);
		}
		updateTimeoutTime(newDuration);
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.eventing.ClientSubscriptionInternal#dispose()
	 */
	@Override
	public void dispose() {
		sink.close();
		WatchDog.getInstance().unregister(this);
		YADSFramework.removeClientSubscription(this);
	}

	// -------------------- TimedEntry --------------------

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.management.TimedEntry#timedOut()
	 */
	@Override
	protected void timedOut() {
		YADSFramework.removeClientSubscription(this);
		sink.getEventListener().subscriptionTimeoutReceived(this);
		sink.close();
	}

	private synchronized void updateTimeoutTime(long duration) {
		if (duration == 0L) {
			timeoutTime = 0L;
		} else {
			timeoutTime = System.currentTimeMillis() + duration;
		}
	}

}
