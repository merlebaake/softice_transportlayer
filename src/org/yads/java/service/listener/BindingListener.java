/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service.listener;

import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.communication.structures.DiscoveryBinding;

public interface BindingListener extends CommunicationStructureListener {

	/**
	 * Will be called if the status of the given {@link DiscoveryBinding} changed to up.
	 * 
	 * @param binding
	 */
	public void announceDiscoveryBindingUp(DiscoveryBinding binding);

	/**
	 * Will be called if the status of the given {@link DiscoveryBinding} changed to down.
	 * 
	 * @param binding
	 */
	public void announceDiscoveryBindingDown(DiscoveryBinding binding);

	/**
	 * Will be called if the status of the given {@link CommunicationBinding} changed to up.
	 * 
	 * @param binding
	 */
	public void announceCommunicationBindingUp(CommunicationBinding binding);

	/**
	 * Will be called if the status of the given {@link CommunicationBinding} changed to down.
	 * 
	 * @param binding
	 */
	public void announceCommunicationBindingDown(CommunicationBinding binding);

}
