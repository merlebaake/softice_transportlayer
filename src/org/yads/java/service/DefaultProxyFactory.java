/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.service;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.dispatch.DefaultDeviceReference;
import org.yads.java.dispatch.MissingMetadataException;
import org.yads.java.dispatch.ServiceReferenceInternal;
import org.yads.java.message.metadata.GetResponseMessage;
import org.yads.java.security.CredentialInfo;
import java.util.HashMap;
import org.yads.java.types.QNameSet;

public class DefaultProxyFactory extends ProxyFactory {

	public DefaultProxyFactory() {

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.ProxyFactory#createProxyService(org.yads.java.dispatch
	 * .ServiceReferenceInternal, org.yads.java.communication.ConnectionInfo,
	 * org.yads.java.structures.ArrayList)
	 */
	@Override
	public Service createProxyService(ServiceReferenceInternal serviceReference, String comManId, HashMap customMData) throws MissingMetadataException {
		return new ProxyService(serviceReference, customMData, comManId);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.ProxyFactory#createProxyDevice(org.yads.java.message
	 * .metadata.GetResponseMessage,
	 * org.yads.java.dispatch.DefaultDeviceReference,
	 * org.yads.java.service.Device, org.yads.java.communication.ConnectionInfo)
	 */
	@Override
	public Device createProxyDevice(GetResponseMessage message, DefaultDeviceReference devRef, Device oldDevice, ConnectionInfo connectionInfo) {
		return new ProxyDevice(message, devRef, oldDevice, connectionInfo);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.ProxyFactory#checkServiceUpdate(org.yads.java.service
	 * .Service, org.yads.java.types.QNameSet,
	 * org.yads.java.security.CredentialInfo)
	 */
	@Override
	public boolean checkServiceUpdate(Service service, QNameSet newPortTypes, CredentialInfo credentialInfo, String comManId) throws MissingMetadataException {
		ProxyService proxyService = (ProxyService) service;
		int oldPortTypesCount = proxyService.getPortTypeCount();
		proxyService.appendPortTypes(newPortTypes, credentialInfo, comManId);
		if (oldPortTypesCount != proxyService.getPortTypeCount()) {
			return true;
		}
		return false;
	}
}
