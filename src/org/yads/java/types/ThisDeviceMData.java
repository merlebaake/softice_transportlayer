/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

public class ThisDeviceMData extends UnknownDataContainer {

	/** HashMap<String language, LocalizedString dpws:FriendlyName> */
	private HashMap	friendlyNames	= new HashMap();

	private String	firmwareVersion	= "";

	private String	serialNumber	= "";

	public ThisDeviceMData() {
		super();
	}

	/**
	 * Copy Constructor. Data structure objects will also be copied.
	 */
	public ThisDeviceMData(ThisDeviceMData metadata) {
		super(metadata);

		if (metadata == null) {
			return;
		}

		friendlyNames.putAll(metadata.friendlyNames);
		firmwareVersion = metadata.firmwareVersion;
		serialNumber = metadata.serialNumber;
	}

	// ---------------------------------------------------

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(StringUtil.formatClassName(getClass()));
		sb.append(" [ friendlyNames=").append(friendlyNames);
		sb.append(", firmwareVersion=").append(firmwareVersion);
		sb.append(", serialNumber=").append(serialNumber);
		sb.append(" ]");
		return sb.toString();
	}

	// ------------------ GETTER -----------------------

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public List getFriendlyNames() {
		return new ArrayList(friendlyNames.values());
	}

	public LocalizedString getFriendlyName(String lang) {
		HashMap friendlyNames = this.friendlyNames;

		if (friendlyNames == null) {
			return null;
		} else {
			return (LocalizedString) friendlyNames.get(lang);
		}
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	// -------------------- SETTER -----------------------

	/**
	 * Gets firmware version
	 * 
	 * @param firmwareVersion
	 */
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	/**
	 * Sets friendly names of device.
	 * 
	 * @param friendlyNames HahshMap with friendly names of device, key must be
	 *            the language string. HashMap<String language, LocalizedString
	 *            dpws:FriendlyName>
	 */
	public void setFriendlyNames(HashMap friendlyNames) {
		this.friendlyNames = friendlyNames;
	}

	/**
	 * Adds friendly name in specified language.
	 * 
	 * @param friendlyName Friendly name of device.
	 */
	public void addFriendlyName(LocalizedString friendlyName) {
		if (friendlyNames == null) {
			friendlyNames = new HashMap();
		}

		friendlyNames.put(friendlyName.getLanguage(), friendlyName);
	}

	/**
	 * Sets serial number
	 * 
	 * @param serialNumber
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

}
