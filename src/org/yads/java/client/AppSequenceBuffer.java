/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.client;

import java.util.LinkedHashMap;
import org.yads.java.types.AppSequence;
import org.yads.java.types.EndpointReference;
import org.yads.java.util.AppSequenceTracker;

public class AppSequenceBuffer {

	protected static int	MAX_BUFFER_SIZE	= 50;

	// endpointReference address String -> AppSequenceTracker
	private LinkedHashMap		buffer;

	/**
	 * Constructor. Creates a new AppSequenceBuffer with the default size.
	 */
	public AppSequenceBuffer() {
		buffer = new LinkedHashMap();
	}

	/**
	 * Constructor. Creates a new AppSequenceBuffer with the given size.
	 * 
	 * @param size number of entries the buffer can hold.
	 */
	public AppSequenceBuffer(int size) {
		buffer = new LinkedHashMap(size);
	}

	/**
	 * Return true if the buffer contains an entry with the given epr, else
	 * false.
	 * 
	 * @param epr the EndpointReference to search for.
	 * @return true entry is found, else false.
	 */
	public synchronized boolean contains(EndpointReference epr) {
		return buffer.containsKey(epr);
	}

	/**
	 * @param epr
	 * @param appSeq
	 * @return true if appSequence is ok.
	 */
	public synchronized boolean checkAndUpdate(EndpointReference epr, AppSequence appSeq) {
		String eprAddr = epr.getAddress().toString();

		AppSequenceTracker appSeqTracker = (AppSequenceTracker) buffer.get(eprAddr);

		if (appSeqTracker != null) {
			return appSeqTracker.checkAndUpdate(appSeq, true);
		}

		// tracker for epr not found
		if (buffer.size() >= MAX_BUFFER_SIZE) {
            // besting: remove first element here, the keyset of the linked hashmap preserves order
			buffer.remove(buffer.keySet().iterator().next());
		}

		buffer.put(eprAddr, new AppSequenceTracker(appSeq));
		return true;
	}
}
