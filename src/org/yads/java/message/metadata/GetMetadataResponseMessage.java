/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.metadata;

import org.yads.java.constants.MessageConstants;
import org.yads.java.description.wsdl.WSDL;
import org.yads.java.message.Message;
import org.yads.java.message.SOAPHeader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.yads.java.types.AttributedURI;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.EndpointReferenceSet;
import org.yads.java.types.HostMData;
import org.yads.java.types.HostedMData;
import org.yads.java.types.RelationshipMData;
import org.yads.java.types.URI;
import org.yads.java.types.URISet;
import org.yads.java.types.UnknownDataContainer;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

/**
 * 
 */
public class GetMetadataResponseMessage extends Message {

	private EndpointReferenceSet	metadataReferences;

	private URISet					metadataLocations;

	private List        			wsdls;

	private RelationshipMData		relationship;

	private HashMap					customMData;

	/**
	 * Creates a new GetMetadataResponse message containing a new created {@link SOAPHeader}. All header- and transfer-related fields are empty and
	 * it is the caller's responsibility to fill them with suitable values.
	 */
	public GetMetadataResponseMessage() {
		this(SOAPHeader.createHeader());
	}

	/**
	 * Creates a new GetMetadataResponse message with the given {@link SOAPHeader}.
	 * 
	 * @param header
	 */
	public GetMetadataResponseMessage(SOAPHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", metadataReferences=").append(metadataReferences);
		sb.append(", metadataLocations=").append(metadataLocations);
		sb.append(", relationship=").append(relationship);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return MessageConstants.GET_METADATA_RESPONSE_MESSAGE;
	}

	public RelationshipMData getRelationship() {
		return relationship;
	}

	public HostMData getHost() {
		return relationship == null ? null : relationship.getHost();
	}

	public HostedMData getHosted(AttributedURI address) {
		if (relationship == null) {
			return null;
		} else {
			Iterator hostedIterator = relationship.getHosted().iterator();
			while (hostedIterator.hasNext()) {
				HostedMData tmp = (HostedMData) hostedIterator.next();
				if (tmp.getEprInfoSet().containsEprAddress(address)) {
					return tmp;
				}
			}
		}
		return null;
	}

	/**
	 * @param relationship the relationship to add
	 */
	public void addRelationship(RelationshipMData relationship) {
		if (this.relationship == null) {
			this.relationship = relationship;
		} else {
			this.relationship.mergeWith(relationship);
		}
	}

	/**
	 * @return the metadataReferences
	 */
	public EndpointReferenceSet getMetadataReferences() {
		return metadataReferences;
	}

	/**
	 * @param metadataReference the metadataReference to add
	 */
	public void addMetadataReference(EndpointReference metadataReference) {
		if (metadataReferences == null) {
			metadataReferences = new EndpointReferenceSet();
		}
		metadataReferences.add(metadataReference);
	}

	/**
	 * @return the metadataLocations
	 */
	public URISet getMetadataLocations() {
		return metadataLocations;
	}

	public void setMetadataLocations(URISet locations) {
		this.metadataLocations = locations;
	}

	/**
	 * @param metadataLocation the metadataLocation to add
	 */
	public void addMetadataLocation(URI metadataLocation) {
		if (metadataLocations == null) {
			metadataLocations = new URISet();
		}
		metadataLocations.add(metadataLocation);
	}

	/**
	 * @return the wsdls
	 */
	public List getWSDLs() {
		return wsdls;
	}

	/**
	 * @param wsdl the wsdl to add
	 */
	public void addWSDL(WSDL wsdl) {
		if (wsdls == null) {
			wsdls = new ArrayList();
		}
		wsdls.add(wsdl);
	}

	/**
	 * @param container instance of the type {@link UnknownDataContainer}
	 */
	public void addCustomMData(String communicationManagerId, UnknownDataContainer container) {
		ArrayList tempMetaData = null;
		if (customMData == null) {
			customMData = new HashMap();
		} else {
			tempMetaData = (ArrayList) customMData.get(communicationManagerId);
		}

		if (tempMetaData == null) {
			tempMetaData = new ArrayList();
			customMData.put(communicationManagerId, tempMetaData);
		}
		tempMetaData.add(container);
	}

	/**
	 * @param customMData {@link HashMap} of CustomMData
	 */
	public void setCustomMData(HashMap customMData) {
		this.customMData = customMData;
	}

	/**
	 * @return the instance of the typ CustomMData
	 */
	public HashMap getCustomMData() {
		return customMData;
	}

}
