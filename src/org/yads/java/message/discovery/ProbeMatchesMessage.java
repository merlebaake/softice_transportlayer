/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.message.discovery;

import java.util.ArrayList;
import java.util.List;
import org.yads.java.constants.MessageConstants;
import org.yads.java.message.SOAPHeader;
import org.yads.java.types.EndpointReference;
import org.yads.java.util.SimpleStringBuilder;
import org.yads.java.util.StringUtil;
import org.yads.java.util.Toolkit;

/**
 * 
 */
public class ProbeMatchesMessage extends SignableMessage {

	private List	probeMatches;

	/**
	 * Creates a new probeMatches message with a new created discovery- {@link SOAPHeader}.
	 */
	public ProbeMatchesMessage() {
		this(MessageWithDiscoveryData.createDiscoveryHeader());
	}

	/**
	 * Creates a new ProbeMatches message containing a {@link SOAPHeader} . All
	 * header- and discovery-related fields are empty and it is the caller's
	 * responsibility to fill them with suitable values.
	 * 
	 * @param header
	 */
	public ProbeMatchesMessage(SOAPHeader header) {
		super(header);
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		SimpleStringBuilder sb = Toolkit.getInstance().createSimpleStringBuilder(StringUtil.formatClassName(getClass()));
		sb.append(" [ header=").append(header);
		sb.append(", inbound=").append(inbound);
		sb.append(", probeMatches=").append(probeMatches);
		sb.append(" ]");
		return sb.toString();
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.Message#getType()
	 */
	@Override
	public int getType() {
		return MessageConstants.PROBE_MATCHES_MESSAGE;
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.message.discovery.ProbeMatchesMessage#
	 * getProbeMatch(int)
	 */
	public ProbeMatch getProbeMatch(int index) {
		return probeMatches == null ? null : (ProbeMatch) probeMatches.get(index);
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.message.discovery.ProbeMatchesMessage#
	 * getProbeMatchCount()
	 */
	public int getProbeMatchCount() {
		return probeMatches == null ? 0 : probeMatches.size();
	}

	/*
	 * (non-Javadoc)
	 * @seeorg.yads.java.communication.message.discovery.ProbeMatchesMessage#
	 * getProbeMatches()
	 */
	public List getProbeMatches() {
		return probeMatches;
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.message.discovery.ProbeMatchesMessage#isEmpty ()
	 */
	public boolean isEmpty() {
		return probeMatches == null || probeMatches.isEmpty();
	}

	public void addProbeMatch(ProbeMatch probeMatch) {
		if (probeMatch == null) {
			return;
		}
		if (probeMatches == null) {
			probeMatches = new ArrayList();
		}
		probeMatches.add(probeMatch);
	}

	public EndpointReference getEndpointReference() {
		if (probeMatches.size() > 0) {
			return ((ProbeMatch) probeMatches.iterator().next()).getEndpointReference();
		}
		return null;
	}

}
