/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.description;

import java.io.IOException;
import java.io.InputStream;

import org.yads.java.description.wsdl.WSDL;
import org.yads.java.security.CredentialInfo;
import org.yads.java.types.URI;
import org.yads.java.xmlpull.v1.XmlPullParser;
import org.yads.java.xmlpull.v1.XmlPullParserException;

/**
 *
 */
public interface DescriptionParser {

	/**
	 * @param in
	 * @param fromUri the URI pointing to the WSDL document being parsed
	 * @param targetNamespace if <code>null</code>, then this is the top-level
	 *            WSDL file (i.e. not an import)
	 * @param loadReferencedFiles if <code>true</code>, other WSDL and XML
	 *            Schema files referenced by the parsed WSDL will be parsed
	 *            recursively, too
	 * @return WSDL
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public WSDL parse(InputStream in, URI fromUri, CredentialInfo credentialInfo, String targetNamespace, boolean loadReferencedFiles, String comManId) throws XmlPullParserException, IOException;

	/**
	 * @param parser
	 * @param fromUri the URI pointing to the WSDL document being parsed
	 * @param targetNamespace if <code>null</code>, then this is the top-level
	 *            WSDL file (i.e. not an import)
	 * @param loadReferencedFiles if <code>true</code>, other WSDL and XML
	 *            Schema files referenced by the parsed WSDL will be parsed
	 *            recursively, too
	 * @return WSDL
	 * @throws XmlPullParserException
	 * @throws IOException
	 */
	public WSDL parse(XmlPullParser parser, URI fromUri, CredentialInfo credentialInfo, String targetNamespace, boolean loadReferencedFiles, String comManId) throws XmlPullParserException, IOException;

}
