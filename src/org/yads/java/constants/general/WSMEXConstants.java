/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.constants.general;

public interface WSMEXConstants {

	/** The default prefix for the WSMEX namespace. */
	public static final String	WSX_NAMESPACE_PREFIX		= "wsx";

	public static final String	WSX_ELEM_GETMETADATA		= "GetMetadata";

	/** "Dialect". */
	public static final String	WSX_ELEM_DIALECT			= "Dialect";

	/** "Metadata". */
	public static final String	WSX_ELEM_METADATA			= "Metadata";

	/** "MetadataSection". */
	public static final String	WSX_ELEM_METADATASECTION	= "MetadataSection";

	/** "MetadataReference". */
	public static final String	WSX_ELEM_METADATAREFERENCE	= "MetadataReference";

	/** "Identifier". */
	public static final String	WSX_ELEM_IDENTIFIER			= "Identifier";

	/** "Location". */
	public static final String	WSX_ELEM_LOCATION			= "Location";

	public static final String	WSX_DIALECT_WSDL			= "http://schemas.xmlsoap.org/wsdl/";

	// Missing slash after wsdl, we still should accept it for compatibility
	public static final String	WSX_DIALECT_WSDL_WRONG		= "http://schemas.xmlsoap.org/wsdl";

}
