/*******************************************************************************
 * Copyright (c) 2016 Andreas Besting (info@besting-it.de)
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *
 *******************************************************************************/

package org.yads.java.structures;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author besting
 */
public class ContainerUtils {
    
    public static synchronized <K extends Object, V extends Object> void add(HashMap<K, HashSet<V>> map, K key, V value) {
        if (!map.containsKey(key))
            map.put(key, new HashSet<V>());
        map.get(key).add(value);
    }
    
}
