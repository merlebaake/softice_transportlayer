/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.dispatch;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.security.SecurityKey;
import org.yads.java.service.ServiceReferenceFactory;
import org.yads.java.types.EndpointReference;
import org.yads.java.types.HostedMData;

public class DefaultServiceReferenceFactory extends ServiceReferenceFactory {

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.ServiceReferenceFactory#newServiceReference(org
	 * .yads.java.security.SecurityKey, org.yads.java.types.HostedMData,
	 * org.yads.java.communication.ConnectionInfo)
	 */
	@Override
	public ServiceReferenceInternal newServiceReference(SecurityKey securityKey, HostedMData hosted, ConnectionInfo connectionInfo, String comManId) {
		return new DefaultServiceReference(securityKey, hosted, connectionInfo, comManId);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.ServiceReferenceFactory#newServiceReference(org
	 * .yads.java.types.EndpointReference, org.yads.java.security.SecurityKey,
	 * java.lang.String)
	 */
	@Override
	public ServiceReferenceInternal newServiceReference(EndpointReference epr, SecurityKey key, String comManId) {
		return new DefaultServiceReference(epr, key, comManId);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.service.ServiceReferenceFactory#newServiceReference(org
	 * .yads.java.service.reference.ServiceReference,
	 * org.yads.java.security.SecurityKey)
	 */
	@Override
	public ServiceReferenceInternal newServiceReference(DefaultServiceReference oldServRef, SecurityKey newSecurity) {
		return new DefaultServiceReference(oldServRef, newSecurity);
	}

}
