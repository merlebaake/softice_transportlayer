/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.configuration;

import java.util.HashMap;

/**
 * Class handles device properties.
 */
public class DevicesPropertiesHandler implements PropertiesHandler {

	private HashMap							devProps			= new HashMap();

	private DeviceProperties				buildUpProperties	= null;

	/** default properties for all devices */
	private DeviceProperties				defaultProperties	= null;

	private static String					className			= null;

	private static DevicesPropertiesHandler	instance			= null;

	// -------------------------------------------------------

	DevicesPropertiesHandler() {
		super();
		className = this.getClass().getName();
	}

	/**
	 * Returns instance of the devices properties handler.
	 * 
	 * @return the singleton instance of the devices properties
	 */
	public static DevicesPropertiesHandler getInstance() {
		if (instance == null) {
			instance = (DevicesPropertiesHandler) Properties.forClassName(Properties.DEVICES_PROPERTIES_HANDLER_CLASS);
		}
		return instance;
	}

	/**
	 * Returns class name if object of this class has already been created, else
	 * null.
	 * 
	 * @return Class name if object of this class has already been created, else
	 *         null.
	 */
	public static String getClassName() {
		return className;
	}

	// -------------------------------------------------------------

	/*
	 * (non-Javadoc)
	 * @see
	 * org.yads.java.configuration.PropertiesHandler#setProperties(org.yads.
	 * java.configuration.PropertyHeader, org.yads.java.configuration.Property)
	 */
	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (Properties.HEADER_SECTION_DEVICES.equals(header)) {
			// Properties of "Devices" Section, default for devices
			if (defaultProperties == null) {
				defaultProperties = new DeviceProperties();
			}

			defaultProperties.addProperty(property);
		}

		else if (Properties.HEADER_SUBSECTION_DEVICE.equals(header)) {
			// Properties of "Device" Section
			if (buildUpProperties == null) {
				if (defaultProperties != null) {
					buildUpProperties = new DeviceProperties(defaultProperties);
				} else {
					buildUpProperties = new DeviceProperties();
				}
			}

			buildUpProperties.addProperty(property);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.configuration.PropertiesHandler#finishedSection(int)
	 */
	@Override
	public void finishedSection(int depth) {
		if (depth == 2 && buildUpProperties != null) {
			// initialize DeviceProperties
			if (!buildUpProperties.getConfigurationId().equals(DeviceProperties.DEFAULT_CONFIGURATION_ID)) {
				Integer id = buildUpProperties.getConfigurationId();

				devProps.put(id, buildUpProperties);
			}
			buildUpProperties = null;
		} else if (depth <= 1) {
			// remove all management structure, it is not used anymore
			defaultProperties = null;
			buildUpProperties = null;
		}
	}

	/**
	 * Gets device properties by configuration id.
	 * 
	 * @param configurationId
	 * @return device properties
	 */
	public DeviceProperties getDeviceProperties(Integer configurationId) {
		return (DeviceProperties) devProps.get(configurationId);
	}
}
