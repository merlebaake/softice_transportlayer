/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class EventingProperties implements PropertiesHandler {

	public static final String	PROP_CONFIGURATION_ID	= Properties.PROP_CONFIGURATION_ID;

	public static final String	PROP_BINDING			= Properties.PROP_BINDING;

	private Integer				tmpConfigurationId		= null;

	private List                tmpBindings				= new ArrayList(1);

	/** map: ConfigurationId<Integer> => Bindings<List of Integer> */
	private static HashMap		map_CID_2_Bindings		= new HashMap();

	/**
	 * Constructor.
	 */
	public EventingProperties() {
		super();
	}

	/**
	 * Returns instance of the eventing properties handler.
	 * 
	 * @return the singleton instance of the eventing properties
	 */
	public static EventingProperties getInstance() {
		return (EventingProperties) Properties.forClassName(Properties.EVENTING_PROPERTIES_HANDLER_CLASS);
	}

	// -----------------------------------------------------------

	@Override
	public void finishedSection(int depth) {
		if (depth == 2) {
			if (tmpConfigurationId != null) {
				map_CID_2_Bindings.put(tmpConfigurationId, tmpBindings);
			}
			tmpBindings = new ArrayList(1);
			tmpConfigurationId = null;
		} else if (depth < 2) {
			tmpBindings = new ArrayList(1);
			tmpConfigurationId = null;
		}
	}

	@Override
	public void setProperties(PropertyHeader header, Property property) {
		if (Properties.HEADER_SECTION_EVENTING.equals(header)) {
			// Properties of "Devices" Section, default for devices
		}

		else if (Properties.HEADER_SUBSECTION_EVENT_SINK.equals(header)) {
			if (PROP_BINDING.equals(property.key)) {
				tmpBindings.add(Integer.valueOf(property.value));
			} else if (PROP_CONFIGURATION_ID.equals(property.key)) {
				tmpConfigurationId = Integer.valueOf(property.value);
			}
		}

	}

	/**
	 * @param configurationId
	 * @return the bindings
	 */
	public List getBindings(Integer configurationId) {
		List bindingIds = (List) map_CID_2_Bindings.get(configurationId);
		List bindings = new ArrayList(bindingIds.size());

		for (Iterator it = bindingIds.iterator(); it.hasNext();) {
			Integer bindingId = (Integer) it.next();
			ArrayList bs = (ArrayList) BindingProperties.getInstance().getCommunicationBinding(bindingId);
			for (int i = 0; i < bs.size(); i++) {
				bindings.add(bs.get(i));
			}
		}
		return bindings;
	}

}
