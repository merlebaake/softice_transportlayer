/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.filter;

import java.io.IOException;

import org.yads.java.types.Memento;

public abstract class AddressFilterEntry {

	public static final String	KEY_ALLOW		= "allow";

	public static final String	KEY_INVERTED	= "inverted";

	protected boolean			inverted		= false;

	protected boolean			allow;

	protected AddressFilterEntry() {}

	public boolean isAllowed() {
		return allow;
	}

	public boolean isInverted() {
		return inverted;
	}

	protected boolean calculateInversion(boolean value) {
		return inverted ^ value;
	}

	/**
	 * @param ipAddress the {@link IPAddress} to check
	 * @return true if the {@link IPAddress} is allowed by the specified
	 *         IPFilter, false if not
	 */
	public abstract boolean check(Object[] key);

	public void readFromMemento(Memento m) throws IOException {
		allow = m.getBooleanValue(KEY_ALLOW, false);
		inverted = m.getBooleanValue(KEY_INVERTED, false);
	}

	public void saveToMemento(Memento m) {
		m.putValue(KEY_ALLOW, allow);
		m.putValue(KEY_INVERTED, inverted);
	}

}
