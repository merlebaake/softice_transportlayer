/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.filter;

public abstract class IPFilter extends AddressFilterEntry {

	public static final int	FILTER_TYPE_ADDRESS			= 1;

	public static final int	FILTER_TYPE_ADDRESS_RANGE	= 2;

	public static final int	FILTER_TYPE_SUBNET			= 3;

	public static final int	FILTER_TYPE_OWN_ADDRESSES	= 4;

	protected IPFilter() {
		// Memento
	}

	public IPFilter(boolean allow, boolean inverted) {
		super();
		this.allow = allow;
		this.inverted = inverted;
	}

	public abstract int getType();

	protected abstract String getInfo();

	@Override
	public String toString() {
		String allow = this.allow ? "Allow " : "Deny ";
		String inverted = this.inverted ? "not " : "";
		return allow + inverted + getInfo();
	}
}
