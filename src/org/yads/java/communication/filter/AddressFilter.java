/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.filter;

import java.util.Iterator;
import java.util.LinkedList;

public class AddressFilter {

	private LinkedList	addressFilterEntries	= new LinkedList();

	boolean				enabled					= false;

	/**
	 * @param adr
	 * @return true, if the ip is allowed or no filter matches the ip address,
	 *         false otherwise.
	 */
	public boolean isAllowedByFilter(Long[] key) {
		if (!enabled) {
			return true;
		}

		/*
		 * Depending of the order of the addressed the check-method will be
		 * called with every filter. If the address matches, depending on the
		 * status of the filter, the address will pass through or not
		 */
		Iterator addressFilterEntriesIterator = addressFilterEntries.iterator();

		while (addressFilterEntriesIterator.hasNext()) {
			AddressFilterEntry filter = (AddressFilterEntry) addressFilterEntriesIterator.next();
			if (filter.check(key)) {
				return filter.isAllowed();
			}
		}
		/*
		 * If no filter matches the address, it will not pass through
		 */
		return false;
	}

	public boolean addFilterItem(AddressFilterEntry filter) {
		return addressFilterEntries.add(filter);
	}

	public void addFilterItem(int index, AddressFilterEntry filter) {
		addressFilterEntries.add(index, filter);
	}

	public void addFilterItems(LinkedList filterList) {
		for (Iterator iterator = filterList.iterator(); iterator.hasNext();) {
			AddressFilterEntry currentIPFilter = (AddressFilterEntry) iterator.next();
			addressFilterEntries.add(currentIPFilter);
		}
	}

	public LinkedList getFilterList() {
		return addressFilterEntries;
	}

	public AddressFilterEntry getFilterItem(int index) {
		return (AddressFilterEntry) addressFilterEntries.get(index);
	}

	public int getFilterItemCount() {
		return addressFilterEntries.size();
	}

	public boolean removeFilterItem(AddressFilterEntry filter) {
		return addressFilterEntries.remove(filter);
	}

	public AddressFilterEntry removeFilterItem(int index) {
		return (AddressFilterEntry) addressFilterEntries.remove(index);
	}

	public void removeAll() {
		addressFilterEntries.clear();
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
