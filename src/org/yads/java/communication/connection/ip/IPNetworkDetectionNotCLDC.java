/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.connection.ip;

import java.io.IOException;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import org.yads.java.configuration.IPProperties;
import org.yads.java.service.listener.NetworkChangeListener;
import org.yads.java.util.Log;
import org.yads.java.util.WS4DIllegalStateException;

public abstract class IPNetworkDetectionNotCLDC extends IPNetworkDetection {

	private int									startCounter	= 0;

	protected PlatformIPNetworkDetectionUpdater	updater			= new PlatformIPNetworkDetectionUpdater();

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.connection.ip.IPNetworkDetection#
	 * getCanonicalAddress()
	 */
	@Override
	public String getCanonicalAddress(String address) {
		try {
			InetAddress tmpAddr = InetAddress.getByName(address);
			String hostAddress = tmpAddr.getHostAddress();
			if (tmpAddr instanceof Inet6Address && !hostAddress.startsWith("[")) {
				hostAddress = "[" + hostAddress + "]";
			}
			return hostAddress;
		} catch (UnknownHostException e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.yads.java.communication.connection.ip.IPNetworkDetection#
	 * detectInterfaces()
	 */
	@Override
	void detectInterfacesAndAddresses() throws IOException {
		if (Log.isDebug()) {
			Log.debug("Start interface detection...");
		}

		prepareMaps();

		Enumeration nis = NetworkInterface.getNetworkInterfaces();
		while (nis.hasMoreElements()) {
			NetworkInterface niSE = (NetworkInterface) nis.nextElement();

			org.yads.java.communication.connection.ip.NetworkInterface ni = createNetworkInterface(niSE);
			Enumeration addrs = niSE.getInetAddresses();
			while (addrs.hasMoreElements()) {
				InetAddress addr = (InetAddress) addrs.nextElement();
				Long[] key = IPAddress.getKeyForIPAddress(addr.getHostAddress());
				IPAddress ipAddress = getAddress(key[0] == null ? ipv4Addresses : ipv6Addresses, key);

				if (ipAddress == null) {
					ipAddress = new IPAddress(addr.getHostAddress(), addr.isLoopbackAddress(), (addr instanceof Inet6Address), addr.isLinkLocalAddress(), key);

					if (ipAddress.isLoopback() && !ipAddress.isIPv6()) {
						IPv4LoopbackAddress = ipAddress;
					}

					putAddress(ipAddress);
				}
				ipAddress.increaseInterfaceCounter();
				ni.addAddress(ipAddress);
			}
			networkinterfaces.put(ni.getName(), ni);
			updateAddressesAndDiscoveryDomains(ni, true, true);
			if (Log.isDebug()) {
				Log.debug("New Interface found: " + ni);
			}
		}
		if (Log.isDebug()) {
			Log.debug("Interface detection done.");
		}
	}

	@Override
	public void refreshNetworkInterfaces() throws IOException {
		if (Log.isDebug()) {
			Log.debug("Start interface refresh ...");
		}

		if (networkinterfaces == null) {
			checkInitiatedInterfaces();
		}
		HashMap actualInterfaces = new HashMap(networkinterfaces);

		Iterator it = networkChangeListener.keySet().iterator();
		while (it.hasNext()) {
			((NetworkChangeListener) it.next()).startUpdates();
		}

		try {
			Enumeration nis = NetworkInterface.getNetworkInterfaces();
			while (nis.hasMoreElements()) {
				NetworkInterface niSE = (NetworkInterface) nis.nextElement();
				org.yads.java.communication.connection.ip.NetworkInterface newIface = createNetworkInterfaceWithIPAddresses(niSE);

				org.yads.java.communication.connection.ip.NetworkInterface oldIface = (org.yads.java.communication.connection.ip.NetworkInterface) actualInterfaces.remove(newIface.getName());
				if (oldIface != null) {
					oldIface.update(newIface);
				} else {
					addNewInterface(newIface);
				}
			}

			// iterate over remaining interfaces, these one are no longer
			// existing. Therefore delete them
			for (Iterator itIfaces = actualInterfaces.values().iterator(); itIfaces.hasNext();) {
				org.yads.java.communication.connection.ip.NetworkInterface iface = (org.yads.java.communication.connection.ip.NetworkInterface) itIfaces.next();
				iface.removed();
				networkinterfaces.remove(iface.getName());
				if (Log.isDebug()) {
					Log.debug("Delete interface : " + iface);
				}
			}
		} finally {
			it = networkChangeListener.keySet().iterator();
			while (it.hasNext()) {
				((NetworkChangeListener) it.next()).stopUpdates();
			}
		}
	}

	protected void addNewInterface(org.yads.java.communication.connection.ip.NetworkInterface newIface) {

		networkinterfaces.put(newIface.getName(), newIface);
		updateAddressesAndDiscoveryDomains(newIface, true, true);
		if (Log.isDebug()) {
			Log.debug("New interface found: " + newIface);
		}
		Iterator it = networkChangeListener.keySet().iterator();
		while (it.hasNext()) {
			((NetworkChangeListener) it.next()).announceNewInterfaceAvailable(newIface);
		}
	}

	protected org.yads.java.communication.connection.ip.NetworkInterface createNetworkInterfaceWithIPAddresses(NetworkInterface niSE) throws IOException {
		org.yads.java.communication.connection.ip.NetworkInterface newIface = createNetworkInterface(niSE);

		Enumeration addrs = niSE.getInetAddresses();
		while (addrs.hasMoreElements()) {
			InetAddress addr = (InetAddress) addrs.nextElement();
			Long[] key = IPAddress.getKeyForIPAddress(addr.getHostAddress());
			IPAddress ip = getAddress(key[0] == null ? ipv4Addresses : ipv6Addresses, key);
			if (ip == null) {
				ip = getAddress(key[0] == null ? ipv4AddressesNotUseableButInBinding : ipv6AddressesNotUseableButInBinding, key);
				if (ip == null) {
					ip = new IPAddress(addr.getHostAddress(), addr.isLoopbackAddress(), (addr instanceof Inet6Address), addr.isLinkLocalAddress(), key);
					putAddress(ip);
				} else {
					moveIP2InUse(ip);
				}
			}
			newIface.addAddress(ip);
		}
		return newIface;
	}

	protected abstract org.yads.java.communication.connection.ip.NetworkInterface createNetworkInterface(NetworkInterface niSE) throws IOException;

	class PlatformIPNetworkDetectionUpdater implements Runnable {

		volatile boolean	running	= false;

		public PlatformIPNetworkDetectionUpdater() {}

		@Override
		public void run() {
			while (running) {
				try {
					synchronized (this) {
						this.wait(IPProperties.NETWORK_DETECTION_REFRESHING_TIME);
					}
				} catch (InterruptedException e) {
					Log.printStackTrace(e);
				}
				if (running) {
					try {
						refreshNetworkInterfaces();
					} catch (IOException e) {
						Log.printStackTrace(e);
					}
				}
			}
			if (Log.isDebug()) {
				Log.debug("Network refreshing unit stopped");
			}
		}
	}

	@Override
	public void startRefreshNetworkInterfacesThread() {
		synchronized (updater) {
			if (startCounter == 0) {
				startRefreshNetworkInterfacesThreadInternal();
			}
			startCounter++;
		}
	}

	@Override
	public void stopRefreshNetworkInterfacesThread() {
		synchronized (updater) {
			if (startCounter == 0) {
				throw new WS4DIllegalStateException("Refresh network infaces thread is not running");
			}
			startCounter--;
			if (startCounter == 0) {
				stopRefreshNetworkInterfacesThreadInternal();
			}
		}
	}

	protected abstract void startRefreshNetworkInterfacesThreadInternal();

	protected abstract void stopRefreshNetworkInterfacesThreadInternal();
}
