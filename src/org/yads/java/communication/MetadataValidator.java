/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication;

import org.yads.java.types.URI;

public class MetadataValidator {

	public String checkManufacturer(String manufacturer) {
		return null;
	}

	public String checkModelName(String modelName) {
		return null;
	}

	public String checkModelNumber(String modelNumber) {
		return null;
	}

	public String checkFriendlyName(String friendlyName) {
		return null;
	}

	public String checkFirmwareVersion(String firmwareVersion) {
		return null;
	}

	public String checkSerialNumber(String serialNumber) {
		return null;
	}

	public String checkManufacturerUrl(URI manufacturerUrl) {
		return null;
	}

	public String checkModelUrl(URI modelUrl) {
		return null;
	}

	public String checkPresentationUrl(URI presentationUrl) {
		return null;
	}

}
