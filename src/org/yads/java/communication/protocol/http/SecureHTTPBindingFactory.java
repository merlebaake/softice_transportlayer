/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.http;

import org.yads.java.communication.connection.ip.IPAddress;
import org.yads.java.communication.structures.CommunicationBinding;
import org.yads.java.security.CredentialInfo;

public class SecureHTTPBindingFactory extends HTTPBindingFactory {

	public CommunicationBinding createCommunicationBinding(IPAddress ipAddress, int port, String path, String comManId, CredentialInfo credentialInfo) {
		if (credentialInfo == null || credentialInfo == CredentialInfo.EMPTY_CREDENTIAL_INFO) {
			return new HTTPBinding(ipAddress, port, path, comManId);
		} else {
			return new HTTPSBinding(ipAddress, port, path, comManId, credentialInfo);
		}

	}
}
