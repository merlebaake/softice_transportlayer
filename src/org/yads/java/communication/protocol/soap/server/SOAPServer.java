/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.soap.server;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import org.yads.java.YADSFramework;
import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.DPWSCommunicationManager;
import org.yads.java.communication.ProtocolInfo;
import org.yads.java.communication.monitor.MonitorStreamFactory;
import org.yads.java.communication.monitor.MonitoredMessageReceiver;
import org.yads.java.communication.monitor.MonitoringContext;
import org.yads.java.communication.protocol.http.HTTPBinding;
import org.yads.java.communication.protocol.http.HTTPResponse;
import org.yads.java.communication.protocol.http.header.HTTPRequestHeader;
import org.yads.java.communication.protocol.http.server.HTTPRequestHandler;
import org.yads.java.communication.protocol.http.server.HTTPServerManager;
import org.yads.java.communication.protocol.soap.SOAPResponse;
import org.yads.java.communication.protocol.soap.generator.SOAPMessageGeneratorFactory;
import org.yads.java.communication.receiver.MessageReceiver;
import org.yads.java.constants.MIMEConstants;
import org.yads.java.message.Message;
import org.yads.java.util.Log;

/**
 * This class allows the creation of a SOAP Server to handle incoming SOAP
 * message requests.
 */
public class SOAPServer {

	/**
	 * The underlying HTTP server.
	 */
	private HTTPServerManager		server	= null;

	/**
	 * This table contains the created SOAP servers.
	 */
	private static final ConcurrentHashMap servers	= new ConcurrentHashMap();

	/**
	 * Creates the SOAP server with the HTTP server.
	 * 
	 * @param server the underlying HTTP server.
	 */
	private SOAPServer(HTTPServerManager server) {
		this.server = server;
	}

	public synchronized static void stopALLServers() {
        synchronized(servers) {
            for (Iterator it = servers.values().iterator(); it.hasNext();) {
                SOAPServer soapserver = (SOAPServer) it.next();
                try {
                    soapserver.server.unregisterAndStop();
                } catch (IOException e) {
                    Log.error("Unable to close SOAPServer: " + e);
                    Log.printStackTrace(e);
                }
            }            
        }
		servers.clear();
	}

	/**
	 * Returns a SOAP server and the underlying HTTP server for the given
	 * address and port. If no server exists, a new server will be created.
	 * 
	 * @param address the host address for the underlying HTTP server.
	 * @param port the port for the underlying HTTP server.
	 * @return the new SOAP server.
	 * @throws IOException Throws exception if the HTTP server could not listen
	 *             to the given address and port.
	 */
	public synchronized static SOAPServer get(HTTPBinding binding, boolean create) throws IOException {
		SOAPServer soapsrv = (SOAPServer) servers.get(binding.getIpPortKey());
		if (soapsrv != null) {
			binding.checkSecurityCredentialsEquality(soapsrv.getHTTPServer().getBinding());
			return soapsrv;
		}
		if (!create) {
			return null;
		}

		HTTPServerManager server = HTTPServerManager.get(binding, create);
		soapsrv = new SOAPServer(server);
		servers.put(binding.getIpPortKey(), soapsrv);
		return soapsrv;
	}

	public synchronized void unregisterAndStop() throws IOException {
		servers.remove(server.getBinding().getIpPortKey());
		server.unregisterAndStop();
	}

	/**
	 * Registers a HTTP path for a given {@link MessageReceiver}.
	 * <p>
	 * The receiver will receive incoming SOAP messages which match the HTTP path.
	 * </p>
	 * 
	 * @param path HTTP path.
	 * @param receiver the SOAP message receiver
	 */
	public void register(String path, SOAPHandler handler) throws IOException {
		server.register(path, MIMEConstants.CONTENT_TYPE_SOAPXML, handler);
	}

	/**
	 * Removes the registration of a {@link MessageReceiver} for a given HTTP
	 * path.
	 * 
	 * @param defaultPath the HTTP path.
	 * @return the removed {@link MessageReceiver}.
	 */
	public MessageReceiver unregister(HTTPBinding binding) {
		return (SOAPHandler) server.unregister(binding, null, MIMEConstants.CONTENT_TYPE_SOAPXML);
	}

	/**
	 * Returns the underlying HTTP server.
	 * 
	 * @return the HTTP server.
	 */
	public HTTPServerManager getHTTPServer() {
		return server;
	}

	/**
	 * The default HTTP handler which handles SOAP-over-HTTP requests.
	 */
	public static abstract class SOAPHandler implements HTTPRequestHandler, MessageReceiver {

		// key = thread, value = HTTPResponse
		private final ConcurrentHashMap	responses	= new ConcurrentHashMap();

		protected SOAPHandler() {
			super();
		}

		/*
		 * (non-Javadoc)
		 * @see
		 * org.yads.java.communication.protocol.httpx.server.HTTPRequestHandler
		 * #handle(org.yads.java.types.uri.URI,
		 * org.yads.java.communication.protocol.http.HTTPRequestHeader,
		 * java.io.InputStream)
		 */
		@Override
		public final HTTPResponse handle(HTTPRequestHeader header, InputStream body, ConnectionInfo connectionInfo, MonitoringContext context) throws IOException {
			connectionInfo.setCommunicationManagerId(DPWSCommunicationManager.COMMUNICATION_MANAGER_ID);
			/*
			 * Gets the HTTP request body if possible
			 */

			final MessageReceiver r;

			MonitorStreamFactory monFac = YADSFramework.getMonitorStreamFactory();
			if (monFac != null) {
				r = new MonitoredMessageReceiver(this, context);
			} else {
				r = this;
			}

			SOAPMessageGeneratorFactory.getInstance().getSOAP2MessageGenerator().deliverMessage(body, r, connectionInfo, null);

			/*
			 * after delivering the request message, the corresponding response
			 * will be immediately available within field 'response'
			 */
			return (HTTPResponse) responses.remove(Thread.currentThread());
		}

		protected final void respond(int httpStatus, boolean secure, Message responseMessage, ProtocolInfo protocolInfo) {
			/*
			 * this takes care of attachments sufficiently (concerns Invoke and
			 * Fault messages)
			 */

			responses.put(Thread.currentThread(), new SOAPResponse(httpStatus, secure, responseMessage, protocolInfo));
		}

	}

}
