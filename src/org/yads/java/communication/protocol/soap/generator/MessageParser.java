/*
 * Copyright (c) 2009 MATERNA Information & Communications. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html. For further
 * project-related information visit http://www.ws4d.org. The most recent
 * version of the JMEDS framework can be obtained from
 * http://sourceforge.net/projects/ws4d-javame.
 *
 * YADS is a fork of JMEDS 2 beta10b.
 * YADS Fork maintainer: SurgiTAIX AG (C) 2017, Author: Andreas Besting Changes released under EPL.
 * YADS project site: http://sourceforge.net/projects/yadstack
 */
package org.yads.java.communication.protocol.soap.generator;

import java.io.IOException;

import org.yads.java.communication.ConnectionInfo;
import org.yads.java.communication.VersionMismatchException;
import org.yads.java.constants.general.DPWSConstantsHelper;
import org.yads.java.io.xml.ElementParser;
import org.yads.java.message.FaultMessage;
import org.yads.java.message.InvokeMessage;
import org.yads.java.message.SOAPHeader;
import org.yads.java.message.discovery.ByeMessage;
import org.yads.java.message.discovery.HelloMessage;
import org.yads.java.message.discovery.ProbeMatchesMessage;
import org.yads.java.message.discovery.ProbeMessage;
import org.yads.java.message.discovery.ResolveMatchesMessage;
import org.yads.java.message.discovery.ResolveMessage;
import org.yads.java.message.eventing.GetStatusMessage;
import org.yads.java.message.eventing.GetStatusResponseMessage;
import org.yads.java.message.eventing.RenewMessage;
import org.yads.java.message.eventing.RenewResponseMessage;
import org.yads.java.message.eventing.SubscribeMessage;
import org.yads.java.message.eventing.SubscribeResponseMessage;
import org.yads.java.message.eventing.SubscriptionEndMessage;
import org.yads.java.message.eventing.UnsubscribeMessage;
import org.yads.java.message.eventing.UnsubscribeResponseMessage;
import org.yads.java.message.metadata.GetMessage;
import org.yads.java.message.metadata.GetMetadataMessage;
import org.yads.java.message.metadata.GetMetadataResponseMessage;
import org.yads.java.message.metadata.GetResponseMessage;
import org.yads.java.service.OperationDescription;
import org.yads.java.xmlpull.v1.XmlPullParserException;

abstract class MessageParser {

	public abstract SOAPHeader parseSOAPHeader(ElementParser parser, ConnectionInfo connectionInfo) throws XmlPullParserException, IOException, VersionMismatchException;

	public abstract HelloMessage parseHelloMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract RenewMessage parseRenewMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract ByeMessage parseByeMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract ProbeMessage parseProbeMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract ProbeMatchesMessage parseProbeMatchesMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract ResolveMessage parseResolveMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract ResolveMatchesMessage parseResolveMatchesMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract InvokeMessage parseInvokeMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract GetStatusMessage parseGetStatusMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract GetStatusResponseMessage parseGetStatusResponseMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract RenewResponseMessage parseRenewResponseMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract SubscribeMessage parseSubscribeMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract SubscribeResponseMessage parseSubscribeResponseMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract SubscriptionEndMessage parseSubscriptionEndMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract UnsubscribeMessage parseUnsubscribeMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract UnsubscribeResponseMessage parseUnsubscribeResponseMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract GetMessage parseGetMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract GetResponseMessage parseGetResponseMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract GetMetadataMessage parseGetMetadataMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract GetMetadataResponseMessage parseGetMetadataResponseMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, DPWSConstantsHelper helper) throws XmlPullParserException, IOException;

	public abstract FaultMessage parseFaultMessage(SOAPHeader header, ElementParser parser, ConnectionInfo connectionInfo, String actionName, OperationDescription op) throws XmlPullParserException, IOException;

}
